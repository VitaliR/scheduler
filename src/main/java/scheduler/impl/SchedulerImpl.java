package scheduler.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import scheduler.IScheduler;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.WeekScheduleStatus;
import com.epam.scheduler.model.Vacation;
 
public class SchedulerImpl implements IScheduler {

	public Map<Integer, List<Integer>> createSchedule(
			Map<Integer, List<Integer>> grid,Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration,
			int moduleDuration, int weekNumberForEducation, boolean location) {
		Map<Integer, List<Integer>> selfStudy = new HashMap<>();
		for (Map.Entry<Integer, List<Integer>> entry : allModulesIdsWithGroupListWithDuration
				.entrySet()) {
			List<Integer> groupsForSelfStudy = new ArrayList<>();
			Integer moduleId = entry.getKey();
			// список id групп которые записалсь на id модуля
			List<Integer> groupIdSignedOnModuleId = entry.getValue();
			// список который даст информацию о свободных неделях для записи
			// всех групп на модуль
			List<Integer> weeksForEachGroup = new ArrayList<Integer>(
					Collections.nCopies(weekNumberForEducation, 0));
			List<Integer> weeksWeightForAllGroupsSignedToModule = new ArrayList<Integer>(
					Collections.nCopies(weekNumberForEducation, 0));
			// проверяем набралось ли 7 и более групп для проведения вебинара
			// или чего то там
			// если да то начинаем искать свободную позицию в сетке расписания
			// для записи этих групп
			// если нет то модули на которые записались группы уходят на
			// самообучение
			if (groupIdSignedOnModuleId.size() >= 7) {
				// ищем позицию
				for (Integer groupId : groupIdSignedOnModuleId) {
					List<Integer> weeksInsideGroup = grid.get(groupId);
					for (int i = 0; i < weekNumberForEducation; i++) {
						if (weeksInsideGroup.get(i) != 0) {

							weeksWeightForAllGroupsSignedToModule.set(i,
									weeksWeightForAllGroupsSignedToModule
											.get(i) + 1);
						}

						weeksForEachGroup.set(i, weeksForEachGroup.get(i)
								+ weeksInsideGroup.get(i));

					}
				}
				int position = findPositionInSchedulerForModuleWithDuration(
						weekNumberForEducation, moduleDuration,
						weeksForEachGroup);
				/*
				 * первый этап записи модулей по группам если position не равно
				 * -1(т.е. равно любому числу от 0 до n) и (позиция +
				 * продолжительность модуля) не выходит за продолжительность
				 * периода обучения, то существует такая позиция куда можно
				 * записать все группы на какой-то модуль, если нет, то
				 * переходим ко второму этапу
				 */
				if (position != -1
						&& (position + moduleDuration) <= weekNumberForEducation) {
					writeModuleInGroupByPosition(groupIdSignedOnModuleId,
							position, moduleDuration, moduleId, grid, gridForScheduleWeekStatus,location);
					// второй этап.
					// есть вероятность того, что например, из 10 человек у
					// одного не получается,
					// но без этого человека можно проводить занятия с
					// оставшимися 9
					// по условию главное чтоб было 7 и более человек и тогда
					// можно проводить занятия,
					// те люди у которых не получилось, изучают модули
					// самостоятельно

				} else if (position == -1
						|| (position != -1 && (position + moduleDuration) > weekNumberForEducation)) {
					int minWeightPosition = 0;
					int minWeight = weeksWeightForAllGroupsSignedToModule
							.get(0);

					// находим позицию с минимальный весом
					for (int i = 0; i < weekNumberForEducation; i++) {
						if (weeksWeightForAllGroupsSignedToModule.get(i) < minWeight) {
							minWeight = weeksWeightForAllGroupsSignedToModule
									.get(i);
							minWeightPosition = weeksWeightForAllGroupsSignedToModule
									.get(i);
						}

					}

					int bestPositionForModules = 0;
					int maxCountGroup = 0;
					for (int i = minWeightPosition; i < weekNumberForEducation; i++) {
						int countGroup = 0;
						for (Integer groupId : groupIdSignedOnModuleId) {
							boolean flag = true;
							List<Integer> weeksPerGroup = grid.get(groupId);
							for (int j = 0; j < moduleDuration; j++) {
								if (((i + moduleDuration) > weekNumberForEducation)
										|| (weeksPerGroup.get(i + j) != 0)) {
									flag = false;
									break;
								}
							}
							if (flag == true) {
								countGroup++;
							}

						}
						if (countGroup > maxCountGroup) {
							bestPositionForModules = i;
							maxCountGroup = countGroup;
						}
					}
					if (maxCountGroup >= 7) {
						List<Integer> withoutSelfStudyGroups = new ArrayList<>();
						for (Integer groupId : groupIdSignedOnModuleId) {
							boolean flag = true;
							List<Integer> weeksPerGroup = grid.get(groupId);
							for (int i = 0; i < moduleDuration; i++) {
								if (((bestPositionForModules + moduleDuration) > weekNumberForEducation)
										|| (weeksPerGroup
												.get(bestPositionForModules + i) != 0)) {
									flag = false;
									break;
								}
							}
							if (flag == true) {
								withoutSelfStudyGroups.add(groupId);
							} else {
								groupsForSelfStudy.add(groupId);
							}
						}
						for (Integer groupId : withoutSelfStudyGroups) {
							List<Integer> weeksPerGroup = grid.get(groupId);
							List<WeekScheduleStatus> weeksScheduleStatusPerGroup = gridForScheduleWeekStatus.get(groupId);
							if (((bestPositionForModules + moduleDuration) <= weekNumberForEducation)
									&& (maxCountGroup >= 7)) {
								if (location) {
									for (int i = 0; i < moduleDuration; i++) {
										weeksPerGroup.set(
												bestPositionForModules + i,
												moduleId);
										weeksScheduleStatusPerGroup.set(bestPositionForModules+i,WeekScheduleStatus.LECTURE);
									}
								} else {
									for (int i = 0; i < moduleDuration; i++) {
										weeksPerGroup.set(
												bestPositionForModules + i,
												moduleId);
										weeksScheduleStatusPerGroup.set(bestPositionForModules+i,WeekScheduleStatus.WEBINAR);
									}
								}
							} else {
								groupsForSelfStudy.add(groupId);
							}
							selfStudy.put(moduleId, groupsForSelfStudy);
						}
					} else {
						selfStudy.put(moduleId, groupIdSignedOnModuleId);
					}
					// последний этап ... модули на самообучение
				} else {
					selfStudy.put(moduleId, groupIdSignedOnModuleId);
				}
			} else {
				selfStudy.put(moduleId, groupIdSignedOnModuleId);
			}
		}
		return selfStudy;
	}

	private void writeModuleInGroupByPosition(
			List<Integer> groupIdSignedOnModuleId, int position,
			int moduleDuration, int moduleId, Map<Integer, List<Integer>> grid, Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			boolean location) {
		for (Integer groupId : groupIdSignedOnModuleId) {
			List<Integer> weeksPerGroup = grid.get(groupId);
			List<WeekScheduleStatus> weeksScheduleStatusPerGroup = gridForScheduleWeekStatus.get(groupId);
			int start = position;
			if (location) {
				for (int i = 1; i <= moduleDuration; i++) {
					weeksPerGroup.set(start, moduleId);
					weeksScheduleStatusPerGroup.set(start,WeekScheduleStatus.LECTURE);
					start++;
				}
			} else {
				for (int i = 1; i <= moduleDuration; i++) {
					weeksPerGroup.set(start, moduleId);
					weeksScheduleStatusPerGroup.set(start,WeekScheduleStatus.WEBINAR);
					start++;
				}
			}
		}
	}

	public Map<Integer, List<Integer>> fillSelfStudyModules(
			Map<Integer, List<Integer>> grid, Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			Map<Integer, List<Integer>> selfStudy, int moduleDuration,
			int weekNumberForAducation) {
		Map<Integer, List<Integer>> selfStudyNotWriten = new HashMap<>();
		if (!selfStudy.isEmpty()) {

			for (Map.Entry<Integer, List<Integer>> entry : selfStudy.entrySet()) {
				Integer moduleId = entry.getKey();
				List<Integer> selfStudyModulesWichCantBeWritenInToSchedule = new ArrayList<>();
				// список id групп которые записалсь на id модуля
				List<Integer> groupIdSignedOnModuleId = entry.getValue();
				// список который даст информацию о свободных неделях для записи
				// всех групп на модуль
				for (Integer groupId : groupIdSignedOnModuleId) {
					List<Integer> weeksInsideGroup = grid.get(groupId);
					List<WeekScheduleStatus> weeksScheduleStatusPerGroup = gridForScheduleWeekStatus.get(groupId);
					int positionIn = findPositionInSchedulerForModuleWithDuration(
							weekNumberForAducation, moduleDuration,
							weeksInsideGroup);
					if (positionIn != -1
							&& (positionIn + moduleDuration) <= weekNumberForAducation) {
						int startIn = positionIn;
						for (int i = 1; i <= moduleDuration; i++) {
							weeksInsideGroup.set(startIn, moduleId);
							weeksScheduleStatusPerGroup.set(startIn,WeekScheduleStatus.SELFSTUDY);
							startIn++;
						}
					} else {
						selfStudyModulesWichCantBeWritenInToSchedule
								.add(groupId);
					}
					// группы которые невозвожно записать в расписание
					// т.к. были "раздроблены"
					selfStudyNotWriten.put(moduleId,
							selfStudyModulesWichCantBeWritenInToSchedule);
				}
			}
		}
		return selfStudyNotWriten;
	}

	// поиск позиции куда можно записать модуль определенной продолжительности
	private int findPositionInSchedulerForModuleWithDuration(
			int weekNumberForAducation, int moduleDuration,
			List<Integer> weeksForEachGroup) {
		int count = 0;
		int position = -1;
		for (int j = 0; j < weekNumberForAducation; j++) {
			int zero = weeksForEachGroup.get(j);

			if (zero == 0) {
				if (position == -1) {
					position = j;
				}
				count++;
				if (count == moduleDuration) {
					j = weekNumberForAducation;
					break;
				}
			} else {
				position = -1;
				count = 0;
			}
		}
		return position;
	}

	public void fillVacationInSchedulerGrid(Map<Integer, List<Integer>> grid,
			List<Group> groups,Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus ) {
		for (Group group : groups) {
			List<Integer> weeksPerGroup = grid.get(group.getGroupId());
			List<Vacation> vacationsPerGroup = group.getVacations();
			List<WeekScheduleStatus> weekScheduleStatusPerGroupVacation = gridForScheduleWeekStatus.get(group.getGroupId());
			if (vacationsPerGroup != null) {
				for (Vacation vacation : vacationsPerGroup) {
					weeksPerGroup.set(vacation.getId() - 1, -100000000);
					weekScheduleStatusPerGroupVacation.set(vacation.getId()-1, WeekScheduleStatus.VACATION);
					
				}
			}
		}

	}

	public Map<Integer, List<Integer>> createSchedulerGrid(List<Group> groups,
			int weekNumberForAducation) throws DataException {
		// сетка для групп и n недель
		// т.е. в каждой группе есть список на n недель
		Map<Integer, List<Integer>> greedForSchedule = new HashMap<>();

		List<Integer> weeksForEachGroup;
		List<Group> allGroupsFromDB = groups;

		for (Group group : allGroupsFromDB) {
			weeksForEachGroup = new ArrayList<>();
			for (int j = 1; j <= weekNumberForAducation; j++) {
				weeksForEachGroup.add(0);
			}
			greedForSchedule.put(group.getGroupId(), weeksForEachGroup);
		}

		return greedForSchedule;
	}
	
	public Map<Integer, List<WeekScheduleStatus>> createSchedulerGridWeekStatus(List<Group> groups,
			int weekNumberForAducation) throws DataException {
		// сетка для групп и 10 недель
		// т.е. в каждой группе есть список на n недель
		Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus = new HashMap<>();

		List<WeekScheduleStatus> weeksForEachGroup;
		List<Group> allGroupsFromDB = groups;

		for (Group group : allGroupsFromDB) {
			weeksForEachGroup = new ArrayList<>();
			for (int j = 1; j <= weekNumberForAducation; j++) {
				weeksForEachGroup.add(WeekScheduleStatus.FREE);
			}
			gridForScheduleWeekStatus.put(group.getGroupId(), weeksForEachGroup);
		}

		return gridForScheduleWeekStatus;
	}
	public TreeMap<Integer, List<Integer>> sortByGroups(
			Map<Integer, List<Integer>> map) {
		ValueListBySizeComparator vlbs = new ValueListBySizeComparator(map);
		TreeMap<Integer, List<Integer>> sorted = new TreeMap<>(vlbs);
		sorted.putAll(map);
		return sorted;
	}

	public TreeMap<Integer, List<Integer>> sortGroupsByModulesWithDuration(
			List<Group> groups, List<Module> modules, int durationForModule)
			throws DataException {
		// достаю все группы из БД в которых есть списки модулей на которые они
		// записались
		List<Group> allGroupsFromDB = groups;
		// создаем Мар размером
		// равным колличеству строк в таблице с модулями
		// ключ это id модуля, а значение это список id групп, которые
		// записались на данный модуль
		Map<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration = new HashMap<Integer, List<Integer>>();
		// задаю колличество всех модулей
		List<Integer> groupsIdSList;
		for (Module moduleFromDB : modules) {
			
			groupsIdSList = new ArrayList<>();
			for (Group group : allGroupsFromDB) {
				List<Module> listOfModulesByCurrentGroup = group.getModules();
				for (Module module : listOfModulesByCurrentGroup) {
					if ((durationForModule == module.getDuration())
							&& (module.equals(moduleFromDB))) {
						groupsIdSList.add(group.getGroupId());
					}

				}
			}
			if (!groupsIdSList.isEmpty()) {
				allModulesIdsWithGroupListWithDuration.put(
						moduleFromDB.getModuleId(), groupsIdSList);
			}
		}

		ValueListBySizeComparator vlbs = new ValueListBySizeComparator(
				allModulesIdsWithGroupListWithDuration);
		TreeMap<Integer, List<Integer>> sorted = new TreeMap<>(vlbs);
		sorted.putAll(allModulesIdsWithGroupListWithDuration);
		return sorted;
	}

}

class ValueListBySizeComparator implements Comparator<Integer> {
	Map<Integer, List<Integer>> map;

	public ValueListBySizeComparator(Map<Integer, List<Integer>> map) {
		this.map = map;
	}

	public int compare(Integer a, Integer b) {
		if (map.get(a).size() >= map.get(b).size()) {
			return -1;
		} else {
			return 1;
		} // returning 0 would merge keys
	}
}
