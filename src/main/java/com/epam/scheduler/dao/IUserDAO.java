package com.epam.scheduler.dao;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IUserDAO {

    public User findUser(String email) throws DataException;
    public List<User> findListUsers(int position, int count) throws DataException;
    public List<String> findExistingEmails(List<String> emails) throws DataException;
    public void addUser(String email,  String roleName) throws DataException, NoSuchAlgorithmException;
    public void addListUser(List<String> list, String roleName) throws DataException, NoSuchAlgorithmException;
    public void updateUser(User user) throws DataException;
    public int findCountUsers() throws DataException;
    public List<User> getAllUserByRole(Role role) throws DataException;
}
