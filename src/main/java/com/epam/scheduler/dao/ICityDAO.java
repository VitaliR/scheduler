package com.epam.scheduler.dao;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.City;

public interface ICityDAO {
	 public City findCityById(int id) throws DataException;
    public City findCity(String name) throws DataException;
    public List<City> getAllCity() throws DataException;
}
