package com.epam.scheduler.dao;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.Module;

public interface IConfigBaseDataOfScheduleDAO {

	public int getQuarterDuration () throws DataException;
	public void modificationVacation(int durationId) throws DataException;
	public void deleteOfficeByStreet(String street)throws DataException;
	public void addNewOffice(Address cityId)throws DataException;
	public Module changeModule(Module module) throws DataException;
	public void deleteModuleByListId(int[] moduleId)  throws DataException;
	public int addModule(Module module) throws DataException;
	public boolean isEmptyGroupTable() throws DataException;
	public boolean isThereOffice(String string) throws DataException;
	public void deleteAllMentors()throws DataException;
	public void deleteAllGroups() throws DataException;
	public int getUserByAddressId(Address address)throws DataException;
	public List<String> listDeleteMentors(String[] mentorList) throws DataException;
	public List<String> deleteTrainerList(String[] trainerList) throws DataException;

}
