package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.scheduler.dao.IModuleDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Module;

public class ModuleDAOImpl implements IModuleDAO {

	private final static String GET_MODULE_BY_ID = "FROM Module module where module.moduleId = :moduleId ";

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Module> getAllModules() throws DataException {
		List<Module> modules = new ArrayList<Module>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Module.class);
			criteria.addOrder(Order.asc("name"));
			modules = criteria.list();
			return modules;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public List<Module> getModuleById(int[] moduleId) throws DataException {
		List<Module> modules = new ArrayList<>();
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					GET_MODULE_BY_ID);
			for (int i = 0; i < moduleId.length; i++) {
				query.setParameter("moduleId", moduleId[i]);
				modules.add((Module) query.uniqueResult());
			}
			Collections.sort(modules, new Comparator<Module>() {
				public int compare(Module m1, Module m2) {
					int result = m1.getModuleId() - m2.getModuleId();
					return result;
				}
			});
			return modules;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public Module findModuleById(int moduleId) throws DataException {
		try {
			Module module = (Module) sessionFactory.getCurrentSession().load(
					Module.class, moduleId);
			return module;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}
}
