package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.scheduler.dao.ISegmentDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Segment;

public class SegmentDAOImpl implements ISegmentDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Segment> getAllSegments() throws DataException {
		List<Segment> segments = new ArrayList<>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Segment.class);
			segments = criteria.list();
			return segments;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}


}
