package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.scheduler.dao.IGroupDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.User;
import com.epam.scheduler.model.Vacation;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

public class GroupDAOImpl implements IGroupDAO {

	private final static String GET_ALL_GROUPS_BY_USER = "FROM Group group WHERE group.user = :user";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addGroup(User user, String groupName, String menteeName,
			String managerName, Address address, List<Module> modules,
			List<Vacation> vocations) throws DataException {
		Group group = new Group(groupName, user, menteeName, managerName,
				address);
		if (vocations != null) {
			group.setVacations(vocations);
		}
		if (modules != null) {
			group.setModules(modules);
		}
		try {
			sessionFactory.getCurrentSession().merge(group);
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> getAllGroupsByUser(User user) throws DataException {
		List<Group> groups = new ArrayList<>();
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					GET_ALL_GROUPS_BY_USER);
			query.setParameter("user", user);
			groups = query.list();
			return groups;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> getAllGroups() throws DataException {
		List<Group> groups = new ArrayList<>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Group.class).addOrder(
					Order.asc("user"));
			groups = criteria.list();
			return groups;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
		
	}

	@Override
	public void deleteGroupById(int[] groupIds) throws DataException {
		try {
			for (int i = 0; i < groupIds.length; i++) {
				Group group = (Group) sessionFactory.getCurrentSession().load(
						Group.class, groupIds[i]);
				sessionFactory.getCurrentSession().delete(group);
			}
		} catch (HibernateException e) {
			throw new DataException(e);
		}

	}

	@Override
	public Group findGroupById(int groupId) throws DataException {
		try {
			Group group = (Group) sessionFactory.getCurrentSession().load(
					Group.class, groupId);
			return group;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> getAllGroupsByCity(City city) throws DataException {
		List<Group> groups = new ArrayList<>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Group.class);
			criteria.add(Restrictions.eq("address", city.getAddresses()));
			criteria.setProjection(Projections.property("id"));
			criteria.addOrder(Order.desc("user"));
			groups = criteria.list();
			return groups;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> getAllGroupsByAddress(Address address)
			throws DataException {
		List<Group> groups = new ArrayList<>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Group.class);
			criteria.add(Restrictions.eq("address", address));
			criteria.addOrder(Order.desc("user"));
			groups = criteria.list();
			return groups;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public void updateGroup(Group group) throws DataException {
		try {
			sessionFactory.getCurrentSession().merge(group);
		} catch (HibernateException e) {
			throw new DataException(e);
		}
		
	}
}
