package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.scheduler.dao.IAddressDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Segment;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AddressDAOImpl implements IAddressDAO {

	private static final String FIND_ADDRESS = "FROM Address address WHERE address.street = :streetName";
	private static final String GET_ALL_ADREESS_BY_CITY = "FROM Address address WHERE address.city = :city order by address.id";
	private static final String GET_ALL_ADREESS_BY_SEGMENT = "FROM Address address WHERE address.segment = :segment";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Address findAddress(String street) throws DataException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					FIND_ADDRESS);
			query.setParameter("streetName", street);
			return (Address) query.uniqueResult();
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Address> getAllAdressByCity(City city) throws DataException {
		List<Address> adressies = new ArrayList<Address>();
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					GET_ALL_ADREESS_BY_CITY);
			query.setParameter("city", city);
			adressies = query.list();
			return adressies;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Address> getAllAdressBySegmentNumber(Segment segment)
			throws DataException {
		List<Address> adressies = new ArrayList<Address>();
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					GET_ALL_ADREESS_BY_SEGMENT);
			query.setParameter("segment", segment);
			adressies = query.list();
			return adressies;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

}
