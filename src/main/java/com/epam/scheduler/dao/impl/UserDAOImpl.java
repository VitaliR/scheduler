package com.epam.scheduler.dao.impl;

import com.epam.scheduler.dao.IUserDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.RoleType;
import com.epam.scheduler.model.User;
import com.epam.scheduler.util.SHAAlgorithm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
public class UserDAOImpl implements IUserDAO {

	private static final String CHECK_EX_ROWS = "FROM User user WHERE user.email IN :arrEm";
	private static final String GET_USERS_BY_ROLE = "FROM User user where user.role = :role  order by user.email";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User findUser(String email) throws DataException {
		try {
			return (User) sessionFactory.getCurrentSession().get(User.class,
					email.toLowerCase());
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findListUsers(int position, int count)
			throws DataException {
		// temp
		List<User> userList = new ArrayList<User>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(User.class);
			userList = criteria.list();
			session.flush();
			return userList;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findExistingEmails(List<String> emails)
			throws DataException {
		try {
			Session session = sessionFactory.getCurrentSession();
			List<User> exUsers = session.createQuery(CHECK_EX_ROWS)
					.setParameterList("arrEm", emails.toArray()).list();
			List<String> exEmails = new ArrayList<>();
			for (User user : exUsers) {
				exEmails.add(user.getEmail());
			}
			return exEmails;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public void addUser(String email, String roleName) throws DataException, NoSuchAlgorithmException {
		try {
			User user = buildUser(email, roleName);
			sessionFactory.getCurrentSession().merge(user);
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public void addListUser(List<String> list, String roleName)
			throws DataException, NoSuchAlgorithmException {
		try {
			Session session = sessionFactory.getCurrentSession();
			for (int i = 0; i < list.size(); i++) {
				session.save(buildUser(list.get(i), roleName));
			}
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public int findCountUsers() throws DataException {
		try {
			Session session = sessionFactory.getCurrentSession();
			int numberNews = 0;
			Criteria criteria = session.createCriteria(User.class);
			criteria.setProjection(Projections.rowCount());
			Long count = (Long) criteria.uniqueResult();
			numberNews = count.intValue();
			session.flush();
			return numberNews;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public void updateUser(User user) throws DataException {
		try {
			sessionFactory.getCurrentSession().update(user);
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	private User buildUser(String email, String roleName) throws NoSuchAlgorithmException {
		User user = new User();
		user.setEmail(email.toLowerCase());
		user.setPassword(SHAAlgorithm.crypt(email));
		Role role = new Role();
		switch (RoleType.valueOf(roleName.toUpperCase())) {
		case MENTOR:
			role.setRoleId(2);
			break;
		case TRAINER:
			role.setRoleId(3);
			break;
		default:
			break;
		}
		role.setRoleType(RoleType.valueOf(roleName.toUpperCase()));
		user.setRole(role);
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUserByRole(Role role) throws DataException {
		List<User> userListByRole = new ArrayList<User>();
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					GET_USERS_BY_ROLE);
			query.setParameter("role", role);
			userListByRole = query.list();
			return userListByRole;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

}
