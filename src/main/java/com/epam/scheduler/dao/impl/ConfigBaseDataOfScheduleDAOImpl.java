package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.scheduler.dao.IConfigBaseDataOfScheduleDAO;
import com.epam.scheduler.exception.*;
import com.epam.scheduler.model.*;


public class ConfigBaseDataOfScheduleDAOImpl implements
		IConfigBaseDataOfScheduleDAO {
	Logger log = LoggerFactory.getLogger(ConfigBaseDataOfScheduleDAOImpl.class);
	private final static String DELETE_ALL_ROWS_VACATION = "DELETE FROM Vacation ";
	private final static String GET_QUARTER_DURATION = "select count(*) FROM Vacation ";
	private final static String DELETE_ADDRES_BY_STREET = "select address from Address address where address.street = :street ";
	private final static String GET_COUNT_ROW_GROUP = "select count(*) from Group";
	private final static String GET_OFFICE_BY_STREET = "select address from Address address where address.street = :street";
	private final static String DELETE_ALL_MENTORS = "DELETE FROM User user where user.role = 2 ";
	private final static String DELETE_ALL_GROUPS = "DELETE FROM Group";
	private final static String GET_USER_BY_ADDRESSID = "select count(*) from User user where user.address = :addressId";
	private final static String GET_COUNT_GROUP_BY_USER = "select count(*) from Group group where group.user = :userEmail";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public int getQuarterDuration() throws DataException {
		int count = 0;
		try {
			Session session = sessionFactory.getCurrentSession();

			count = ((Long) session.createQuery(GET_QUARTER_DURATION)
					.setReadOnly(true).uniqueResult()).intValue();

		} catch (HibernateException e) {
			log.error("getQuarterDuration(), ", e);
			throw new InternalServerException();
		}
		return count;
	}

	@Override
	public void modificationVacation(int currentDurationId)
			throws DataException {
		int batchSize = 50;
		try {
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(DELETE_ALL_ROWS_VACATION);
			query.executeUpdate();
			if (batchSize > currentDurationId) {
				batchSize = currentDurationId;
			}
			
			for (int i = 0; i < batchSize; i++) {

				Vacation vacation = new Vacation();
				vacation.setId(i + 1);
				vacation.setWeekNumber(i + 1);
				session.save(vacation);
				if ((i + 1) % currentDurationId == 0) { // 20, same as the JDBC
														// batch
					// size
					// flush a batch of inserts and release memory:
					session.flush();
					session.clear();
				}
			}
		} catch (HibernateException e) {
			log.error("modificationVacation(), ", e);
			throw new InternalServerException();
		}
	}

	@Override
	public void deleteOfficeByStreet(String street) throws DataException {
		try {
			Address streetDelete = (Address) sessionFactory.getCurrentSession()
					.createQuery(DELETE_ADDRES_BY_STREET)
					.setString("street", street).uniqueResult();
			sessionFactory.getCurrentSession().delete(streetDelete);
		} catch (HibernateException e) {
			log.error("deleteOfficeByStreet(), ", e);
			throw new InternalServerException();
		}
	}

	@Override
	public void addNewOffice(Address address) throws DataException {
		try {
			sessionFactory.getCurrentSession().merge(address);
		} catch (HibernateException e) {
			log.error("addNewOffice(), ", e);
			throw new InternalServerException();
		}
	}

	@Override
	public Module changeModule(Module module) throws DataException {
		Module moduleAfterChande = null;
		try {
			Module moduleBeforeChange = (Module) sessionFactory
					.getCurrentSession()
					.get(Module.class, module.getModuleId());
			moduleBeforeChange.setDuration(module.getDuration());
			moduleBeforeChange.setName(module.getName());
			moduleAfterChande = (Module) sessionFactory.getCurrentSession()
					.merge(moduleBeforeChange); // Success!
		} catch (HibernateException e) {
			log.error("changeModule(), ", e);
			throw new InternalServerException();
		}
		return moduleAfterChande;
	}

	@Override
	public void deleteModuleByListId(int[] moduleId) throws DataException {
		try {
			for (int i = 0; i < moduleId.length; i++) {
				Module module = (Module) sessionFactory.getCurrentSession()
						.load(Module.class, moduleId[i]);
				sessionFactory.getCurrentSession().delete(module);
			}
		} catch (HibernateException e) {
			log.error("deleteModuleByListId(), ", e);
			throw new InternalServerException();
		}
	}

	@Override
	public int addModule(Module module) throws DataException {
		int moduleId;
		try {
			moduleId = ((Module) sessionFactory.getCurrentSession().merge(
					module)).getModuleId();
		} catch (HibernateException e) {
			log.error("addModule(), ", e);
			throw new InternalServerException();
		}
		return moduleId;
	}

	@Override
	public boolean isEmptyGroupTable() throws DataException {
		int count = 0;
		try {
			count = ((Long) sessionFactory.getCurrentSession()
					.createQuery(GET_COUNT_ROW_GROUP).uniqueResult())
					.intValue();
		} catch (HibernateException e) {
			log.error("isEmptyGroupTable(), ", e);
			throw new InternalServerException();
		}
		return (0 == count);
	}

	@Override
	public boolean isThereOffice(String street) throws DataException {
		Address testAddress = null;
		try {
			testAddress = (Address) sessionFactory.getCurrentSession()
					.createQuery(GET_OFFICE_BY_STREET)
					.setString("street", street).uniqueResult();

		} catch (HibernateException e) {
			log.error("isThereOffice(), ", e);
			throw new InternalServerException();
		}
		return (testAddress != null);
	}

	@Override
	public void deleteAllMentors() throws DataException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					DELETE_ALL_MENTORS);
			query.executeUpdate();
		} catch (HibernateException e) {
			log.error("deleteAllMentors(), ", e);
			throw new InternalServerException();
		}
	}

	@Override
	public void deleteAllGroups() throws DataException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					DELETE_ALL_GROUPS);
			query.executeUpdate();
		} catch (HibernateException e) {
			log.error("deleteAllGroups(), ", e);
			throw new InternalServerException();
		}
	}

	@Override
	public int getUserByAddressId(Address address) throws DataException {
		int countUsers = 0;
		try {
			countUsers = ((Long) sessionFactory.getCurrentSession()
					.createQuery(GET_USER_BY_ADDRESSID)
					.setParameter("addressId", address).uniqueResult())
					.intValue();
		} catch (HibernateException e) {
			log.error("changeModule(), ", e);
			throw new InternalServerException();
		}
		return countUsers;
	}

	@Override
	public List<String> listDeleteMentors(String[] mentorList)
			throws DataException {
		ArrayList<String> successListMentor = new ArrayList<String>();
		try {
			for (int i = 0; i < mentorList.length; i++) {
				User user = (User) sessionFactory.getCurrentSession().load(
						User.class, mentorList[i]);
				int countGroup = ((Long) sessionFactory.getCurrentSession()
						.createQuery(GET_COUNT_GROUP_BY_USER)
						.setParameter("userEmail", user).uniqueResult())
						.intValue();
				if (0 == countGroup) {
					sessionFactory.getCurrentSession().delete(user);
					successListMentor.add(mentorList[i]);
				}
			}
		} catch (HibernateException e) {
			log.error("listDeleteMentors(), ", e);
			throw new InternalServerException();
		}
		return successListMentor;
	}

	@Override
	public List<String> deleteTrainerList(String[] trainerList)
			throws DataException {
		ArrayList<String> successListTrainer = new ArrayList<String>();
		try {
			for (int i = 0; i < trainerList.length; i++) {
				User user = (User) sessionFactory.getCurrentSession().load(
						User.class, trainerList[i]);

				// You need to now mapping trainer to other entity to check!!!!
				// int countGroup = ((Long) sessionFactory.getCurrentSession()
				// .createQuery(GET_COUNT_GROUP_BY_USER)
				// .setParameter("userEmail", user).uniqueResult()).intValue();
				// if (0 == countGroup) {
				sessionFactory.getCurrentSession().delete(user);
				successListTrainer.add(trainerList[i]);
				// }
			}
		} catch (HibernateException e) {
			log.error("deleteTrainerList(), ", e);
			throw new InternalServerException();
		}
		return successListTrainer;
	}
}
