package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.scheduler.dao.ICityDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.City;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

public class CityDAOImpl implements ICityDAO {

	private static final String FIND_CITY_BY_NAME = "FROM City city WHERE city.name = :cityName";
	private static final String FIND_CITY_BY_ID = "FROM City city WHERE city.cityId = :cityId";

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public City findCity(String name) throws DataException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					FIND_CITY_BY_NAME);
			query.setParameter("cityName", name);
			return (City) query.uniqueResult();
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> getAllCity() throws DataException {
		List<City> cities = new ArrayList<>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(City.class);
			criteria.addOrder(Order.asc("id"));
			cities = criteria.list();
			return cities;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@Override
	public City findCityById(int id) throws DataException {
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					FIND_CITY_BY_ID);
			query.setParameter("cityId", id);
			return (City) query.uniqueResult();
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}
}
