package com.epam.scheduler.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.scheduler.dao.IVacationDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Vacation;

public class VacationDAOImpl implements IVacationDAO {

	private final static String GET_VOCATION_BY_ID = "FROM Vacation vacation where vacation.id = :id ";
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Vacation> getListVocationById(int[] vacation)
			throws DataException {
		List<Vacation> vocations = new ArrayList<>();
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					GET_VOCATION_BY_ID);
			for (int i = 0; i < vacation.length; i++) {
				query.setParameter("id", vacation[i]);
				vocations.add((Vacation) query.uniqueResult());
			}
			Collections.sort(vocations, new Comparator<Vacation>() {
				public int compare(Vacation v1, Vacation v2) {
					int result = v1.getId() - v2.getId();
					return result;
				}
			});
			return vocations;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Vacation> getAllListVacation() throws DataException {
		List<Vacation> vacations = new ArrayList<>();
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Vacation.class);
			vacations = criteria.list();
			return vacations;
		} catch (HibernateException e) {
			throw new DataException(e);
		}
	}

}
