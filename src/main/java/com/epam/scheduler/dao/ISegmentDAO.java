package com.epam.scheduler.dao;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Segment;

public interface ISegmentDAO {
	public List<Segment> getAllSegments() throws DataException;
}
