package com.epam.scheduler.controller;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.logic.IGroupService;
import com.epam.scheduler.logic.IModuleService;
import com.epam.scheduler.logic.IUserService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.User;
import com.epam.scheduler.util.SHAAlgorithm;

@Controller
public class RegistrationController {
	static Logger log = LoggerFactory.getLogger(RegistrationController.class);
	@Autowired
	private IGroupService groupService;
	@Autowired
	private IModuleService moduleService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ICityService cityService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IConfigBaseDataOfScheduleService configService;

	@RequestMapping(value = "/editUserProfile", method = RequestMethod.POST)
	public ModelAndView editUserProfile(
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "street", required = false) String street,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		User user;
		try {
			user = userService.findUser(email.toLowerCase());

			Address address = addressService.findAddress(street);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			// Password

			user.setPassword(SHAAlgorithm.crypt(password));
			user.setAddress(address);
			userService.updateUser(user);
			model.addObject("user", user);
			request.getSession().setAttribute("user", user);
			Role role = user.getRole();
			switch (role.getRoleType()) {
			case ADMIN:
				model.addObject("page", "../body/CuratorScheduleBody.jsp");
				model.setViewName("curator/main/CuratorMainPage");
				break;
			case MENTOR:
				model.addObject("page",
						"/WEB-INF/jsp/mentor/body/MentorScheduleBody.jsp");
				model.addObject("cities", cityService.getAllCity());
				model.addObject("modules", moduleService.getAllModules());
				model.addObject("groups", groupService.getAllGroupsByUser(user));
				model.addObject("weekDuration",
						configService.getQuarterDuration());
				model.setViewName("mentor/main/MentorMainPage");
				break;
			case TRAINER:
				model.addObject("page",
						"/WEB-INF/jsp/trainer/body/TrainerScheduleBody.jsp");
				model.setViewName("trainer/main/TrainerMainPage");
				break;
			default:
				model.setViewName("login");
				break;
			}
		} catch (DataException | NoSuchAlgorithmException e) {
			log.error("editUserProfile", e);
		}
		return model;
	}

	@RequestMapping(value = "/editUserProfileLink", method = RequestMethod.GET)
	public ModelAndView editUserProfileLink(
			@RequestParam(value = "page", required = false) String page,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("page", "/WEB-INF/jsp/editFormBody.jsp");
		User user = (User) request.getSession().getAttribute("user");
		try {
			model.addObject("cities", cityService.getAllCity());

			model.addObject("addressies", user.getAddress().getCity()
					.getAddresses());
			switch (user.getRole().getRoleType()) {
			case ADMIN:
				model.setViewName("curator/main/CuratorMainPage");
				break;
			case MENTOR:
				model.setViewName("mentor/main/MentorMainPage");
				break;
			case TRAINER:
				model.setViewName("trainer/main/TrainerMainPage");
				break;
			default:
				model.setViewName("login");
				break;
			}
		} catch (DataException e) {
			log.error("editUserProfileLink", e);
		}
		return model;
	}

	@RequestMapping(value = "/registerNewUserProfileLink", method = RequestMethod.GET)
	public ModelAndView registerNewUserProfileLink(
			@RequestParam(value = "page", required = false) String page,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		try {
			model.addObject("cities", cityService.getAllCity());
		} catch (DataException e) {
			log.error("registerNewUserProfileLink", e);
		}
		model.setViewName("RegisterFormBody");
		return model;
	}
}
