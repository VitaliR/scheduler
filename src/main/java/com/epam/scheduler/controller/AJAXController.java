package com.epam.scheduler.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.exception.InternalServerException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Module;

@Controller
public class AJAXController {

	public static volatile boolean period = true;
	@Autowired
	private ICityService cityService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IConfigBaseDataOfScheduleService configService;

	@ResponseBody
	@RequestMapping(value = "/getAddressiesByCityId/{cityId}", method = RequestMethod.GET, produces = { "application/json" })
	public String[] getAddressiesInJSON(@PathVariable int cityId)
			throws DataException {
		City city = null;
		String[] streets = null;
		if (cityId != 0) {
			city = cityService.findCityById(cityId);

			streets = new String[city.getAddresses().size()];
			List<Address> addressesSet = city.getAddresses();
			int i = 0;
			for (Address address : addressesSet) {
				streets[i] = address.getStreet();
				i++;
			}
		}
		return streets;
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "/curatorRegistrationPeriodSwitcher", method = RequestMethod.POST, produces = { "application/json" })
	public void curatorRegistrationPeriodSwitcher(@RequestBody String flag) {
		boolean periodSwithcer = Boolean.parseBoolean(flag);
		period = periodSwithcer;
	}

	@ResponseBody
	@RequestMapping(value = "/curatorChangeDurationQuarter/{durationId}", method = RequestMethod.GET, produces = { "application/json" })
	public int changeDurationQuarter(@PathVariable int durationId,
			HttpServletResponse response) throws DataException, IOException {
		int idDuration = 0;
		idDuration = configService.modificationVacation(durationId);
		return idDuration;
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "/curatorChangeModule", method = RequestMethod.POST, headers = "Content-type=application/json")
	public void changeModule(@RequestBody @Valid Module module,
			BindingResult result, HttpServletResponse response)
			throws DataException, IOException {
		if (result.hasErrors()) {
			response.sendError(423);
		} else {
			configService.changeModule(module);
		}

	}

	@ResponseBody
	@RequestMapping(value = "/curatorDeleteMentorList", method = RequestMethod.DELETE, headers = "Content-type=application/json")
	public List<String> deleteMentorList(@RequestBody String[] mentorList,
			HttpServletResponse response) throws DataException, IOException {
		return configService.listDeleteMentors(mentorList);
	}

	@ResponseBody
	@RequestMapping(value = "/curatorDeleteTrainerList", method = RequestMethod.DELETE, headers = "Content-type=application/json")
	public List<String> deleteTrainerList(@RequestBody String[] trainerList,
			HttpServletResponse response) throws DataException, IOException {
		return configService.deleteTrainerList(trainerList);
	}

	@ResponseBody
	@RequestMapping(value = "/curatorDeleteModuleByListId", method = RequestMethod.DELETE, headers = "Content-type=application/json")
	public String curatorDeleteModuleByListId(@RequestBody int[] moduleId,
			HttpServletResponse response) throws DataException, IOException {
		return String.valueOf(configService.deleteModuleByListId(moduleId)); 
	}

	@ResponseBody
	@RequestMapping(value = "/curatorDeleteAllMentors", method = RequestMethod.DELETE, headers = "Content-type=application/json")
	public String deleteAllMentors(HttpServletResponse response)
			throws DataException, IOException {
		return String.valueOf(configService.deleteAllMentors());
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "/curatorDeleteAllGroups", method = RequestMethod.DELETE, headers = "Content-type=application/json")
	public void deleteAllGroups(HttpServletResponse response)
			throws DataException, IOException {
		configService.deleteAllGroups();
	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/curatorAddModule", method = RequestMethod.PUT, headers = "Content-type=application/json")
	public @ResponseBody int addModule(@RequestBody Module moduleId,
			HttpServletResponse response) throws DataException, IOException {
		return configService.addModule(moduleId);
	}

	@ResponseBody
	@RequestMapping(value = "/curatorAddNewOffice", method = RequestMethod.PUT, headers = "Content-type=application/json")
	public String addNewOffice(@RequestBody @Valid Address officeForm,
			BindingResult result, HttpServletResponse response)
			throws DataException, IOException {
		if (result.hasErrors()) {
			response.sendError(423);
		}
		return String.valueOf(configService.addNewOffice(officeForm));
	}

	// delete Office
	@ResponseBody
	@RequestMapping(value = "/curatorDeleteOffice", method = RequestMethod.POST)
	public String deleteOffice(@RequestBody @Valid Address officeForm,
			BindingResult result, HttpServletResponse response)
			throws DataException, IOException {
		System.out.println(officeForm.getStreet());
		if (result.hasErrors()) {
			System.out.println("test");
			response.sendError(423);
		} return String.valueOf(configService.deleteOfficeByStreet(officeForm.getStreet()));
	}
}
