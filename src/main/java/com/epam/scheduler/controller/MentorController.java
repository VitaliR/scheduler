package com.epam.scheduler.controller;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.logic.IGroupService;
import com.epam.scheduler.logic.IModuleService;
import com.epam.scheduler.logic.IVacationService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.RoleType;
import com.epam.scheduler.model.User;
import com.epam.scheduler.model.Vacation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MentorController {
	static Logger log = LoggerFactory.getLogger(MentorController.class);
	@Autowired
	private IGroupService groupService;
	@Autowired
	private IVacationService vacationService;
	@Autowired
	private ICityService cityService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IModuleService moduleService;
	@Autowired
	private IConfigBaseDataOfScheduleService configService;

	@RequestMapping(value = "/mentorCreateGroup", method = RequestMethod.POST)
	public ModelAndView mentorCreateGroup(
			@RequestParam(value = "groupName", required = false) String groupName,
			@RequestParam(value = "menteeName", required = false) String menteeName,
			@RequestParam(value = "managerName", required = false) String managerName,
			@RequestParam(value = "city", required = false) String cityName,
			@RequestParam(value = "street", required = false) String street,
			@RequestParam(value = "module", required = false) int module[],
			@RequestParam(value = "weekVocation", required = false) int[] weekVocation,
			HttpServletRequest request) {
		Address address;
		try {
			address = addressService.findAddress(street);
			User user = (User) request.getSession().getAttribute("user");
			List<Vacation> vacations = null;
			List<Module> modules = null;
			if (weekVocation != null) {
				vacations = vacationService.getListVocationById(weekVocation);
			}
			if (module != null) {
				modules = moduleService.getModuleById(module);
			}
			// adding new group
			groupService.addGroup(user, groupName, menteeName, managerName,
					address, modules, vacations);
			return new ModelAndView("redirect:/mentorCreateGroupLink");
		} catch (DataException e) {
			log.error("mentorCreateGroup", e);
		}
		return new ModelAndView();
	}

	@RequestMapping(value = "/mentorCreateGroupLink", method = RequestMethod.GET)
	public ModelAndView mentorCreateGroupLink(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("page",
				"/WEB-INF/jsp/mentor/body/MentorScheduleBody.jsp");
		User user = (User) request.getSession().getAttribute("user");

		try {
			model.addObject("weekDuration",
					configService.getQuarterDuration());

			model.addObject("groups", groupService.getAllGroupsByUser(user));
			model.addObject("cities", cityService.getAllCity());
			model.addObject("modules", moduleService.getAllModules());
		} catch (DataException e) {
			log.error("mentorCreateGroupLink", e);
		}
		model.setViewName("mentor/main/MentorMainPage");
		return model;
	}

	// delete Group by ids
	@RequestMapping(value = "/mentorDeleteGroup", method = RequestMethod.POST)
	public ModelAndView mentorDeleteGroup(
			@RequestParam(value = "deleteGroup", required = false) int[] groupId,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("redirect:/mentorCreateGroupLink");
		try {
			groupService.deleteGroupById(groupId);
		} catch (DataException e) {
			log.error("mentorDeleteGroup", e);
		}
		return model;
	}

	@RequestMapping(value = "/mentorEditGroupLink", method = RequestMethod.GET)
	public ModelAndView mentorEditGroupLink(
			@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "groupId") int groupId) {
		ModelAndView model = new ModelAndView();
		try {
			Group group = groupService.findGroupById(groupId);
			model.addObject("cities", cityService.getAllCity());

			model.addObject("modules", moduleService.getAllModules());
			Role role = new Role();
			role.setRoleId(2);
			role.setRoleType(RoleType.MENTOR);
			model.addObject("group", group);
			model.addObject("vacations", group.getVacations());
			model.addObject("weekDuration",
					configService.getQuarterDuration());
			model.addObject("addressies", group.getAddress().getCity()
					.getAddresses());
		} catch (DataException e) {
			log.error("mentorEditGroupLink", e);
		}
		model.addObject("page", "../body/MentorEditGroupBody.jsp");
		model.setViewName("mentor/main/MentorMainPage");
		return model;
	}

	@RequestMapping(value = "/mentorEditGroup", method = RequestMethod.POST)
	public ModelAndView mentorEditGroup(
			@RequestParam(value = "groupName", required = false) String groupName,
			@RequestParam(value = "menteeName", required = false) String menteeName,
			@RequestParam(value = "managerName", required = false) String managerName,
			@RequestParam(value = "street", required = false) String street,
			@RequestParam(value = "module", required = false) int module[],
			@RequestParam(value = "weekVocation", required = false) int[] weekVocation,
			@RequestParam(value = "groupId", required = false) int groupId,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("redirect:/mentorCreateGroupLink");
		Address address;
		try {
			address = addressService.findAddress(street);

			List<Vacation> vacations = null;
			List<Module> modules = null;
			if (weekVocation != null) {
				vacations = vacationService.getListVocationById(weekVocation);
			}
			if (module != null) {
				modules = moduleService.getModuleById(module);
			}
			Group group = groupService.findGroupById(groupId);
			group.setName(groupName);
			group.setManager(managerName);
			group.setMentee(menteeName);
			group.setModules(modules);
			group.setAddress(address);
			group.setVacations(vacations);
			groupService.updateGroup(group);
		} catch (DataException e) {
			log.error("mentorEditGroup", e);
		}
		return model;
	}

}
