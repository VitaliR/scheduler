package com.epam.scheduler.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.epam.scheduler.builder.ScheduleBuilder;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.logic.IGroupService;
import com.epam.scheduler.logic.IModuleService;
import com.epam.scheduler.logic.ISchedulerService;
import com.epam.scheduler.logic.ISegmentService;
import com.epam.scheduler.logic.IUserService;
import com.epam.scheduler.logic.IVacationService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.RoleType;
import com.epam.scheduler.model.ScheduleVO;
import com.epam.scheduler.model.Segment;
import com.epam.scheduler.model.User;
import com.epam.scheduler.model.Vacation;
import com.epam.scheduler.parser.IParser;
import com.epam.scheduler.util.UserValidator;

@Controller
public class AdminController {
	static Logger log = LoggerFactory.getLogger(AdminController.class);
	@Autowired
	private IUserService userService;
	@Autowired
	private ICityService cityService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IVacationService vacationService;
	@Autowired
	private IModuleService moduleService;
	@Autowired
	private IGroupService groupService;
	@Autowired
	private ISchedulerService schedulerService;
	@Autowired
	private IConfigBaseDataOfScheduleService configService;
	@Autowired
	private ISegmentService segmentService;
	@Autowired
	private IParser parser;
	@Autowired
	private ScheduleBuilder scheduleBuilder;

	@RequestMapping(value = "/curatorMainPage", method = RequestMethod.GET)
	public ModelAndView curatorMainPage(
			@RequestParam(value = "page", required = false) String page) {
		ModelAndView model;
		if ("mentor".equalsIgnoreCase(page)) {
			model = new ModelAndView("redirect:/curatorMentorBody");
		} else if ("trainer".equalsIgnoreCase(page)) {
			// model.addObject("page", "../body/CuratorTrainerBody.jsp");
			model = new ModelAndView("redirect:/curatorTrainerBody");
		} else if ("group".equalsIgnoreCase(page)) {
			model = new ModelAndView("redirect:/curatorAddGroupLink");
		} else {
			model = new ModelAndView();
			model.addObject("page", "../body/CuratorScheduleBody.jsp");
			model.setViewName("curator/main/CuratorMainPage");
		}

		return model;
	}

	// curatorMentorBody
	@RequestMapping(value = "/curatorMentorBody", method = RequestMethod.GET)
	public ModelAndView curatorMentorBody(
			@RequestParam(value = "page", required = false) String page) {
		ModelAndView model = new ModelAndView();
		Role role = new Role();
		role.setRoleId(2);
		role.setRoleType(RoleType.MENTOR);
		try {
			model.addObject("users", userService.getAllUserByRole(role));
		} catch (DataException e) {
			log.error("curatorMentorBody", e);
		}
		model.addObject("page", "../body/CuratorMentorBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	@RequestMapping(value = "/curatorMentorBodyPOST", method = RequestMethod.POST)
	public ModelAndView curatorMentorBodyPOST(
			@RequestParam(value = "page", required = false) String page,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Role role = new Role();
		role.setRoleId(2);
		role.setRoleType(RoleType.MENTOR);
		try {
			model.addObject("users", userService.getAllUserByRole(role));
		} catch (DataException e) {
			log.error("curatorMentorBodyPOST", e);
		}
		model.addObject("page", "../body/CuratorMentorBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	// Link to addForm Mentor
	@RequestMapping(value = "/curatorAddFormMentor", method = RequestMethod.GET)
	public ModelAndView curatorAddFormMentor(
			@RequestParam(value = "page", required = false) String page) {
		ModelAndView model = new ModelAndView();
		model.addObject("page", "../body/CuratorAddMentorBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	// add new mentor by email or from file
	@RequestMapping(value = "/curatorAddNewMentor", method = RequestMethod.POST)
	public ModelAndView curatorAddNewMentor(
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam(value = "check", required = false) String check,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("forward:/curatorMentorBodyPOST");
		Role role = new Role();
		role.setRoleId(2);
		role.setRoleType(RoleType.MENTOR);
		if ("true".equals(check)) {
			addUserByEmail(email, request, role);
		} else {
			addUserFromFile(file, request, role);
		}
		return model;
	}

	// curatorTrainerBody GET
	@RequestMapping(value = "/curatorTrainerBody", method = RequestMethod.GET)
	public ModelAndView curatorTrainerBody(
			@RequestParam(value = "page", required = false) String page) {
		ModelAndView model = new ModelAndView();
		Role role = new Role();
		role.setRoleId(3);
		role.setRoleType(RoleType.TRAINER);
		try {
			model.addObject("users", userService.getAllUserByRole(role));
		} catch (DataException e) {
			log.error("curatorTrainerBody", e);
		}
		model.addObject("page", "../body/CuratorTrainerBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	// curatorTrainerBody POST
	@RequestMapping(value = "/curatorTrainerBodyPOST", method = RequestMethod.POST)
	public ModelAndView curatorTrainerBodyPOST(
			@RequestParam(value = "page", required = false) String page,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Role role = new Role();
		role.setRoleId(3);
		role.setRoleType(RoleType.TRAINER);
		try {
			model.addObject("users", userService.getAllUserByRole(role));
		} catch (DataException e) {
			log.error("curatorTrainerBodyPOST", e);
		}
		model.addObject("page", "../body/CuratorTrainerBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	// Link to addForm Trainer
	@RequestMapping(value = "/curatorAddFormTrainer", method = RequestMethod.GET)
	public ModelAndView curatorAddFormTrainer(
			@RequestParam(value = "page", required = false) String page) {
		ModelAndView model = new ModelAndView();
		model.addObject("page", "../body/CuratorAddTrainerBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	// add new trainer by email or from file
	@RequestMapping(value = "/curatorAddNewTrainer", method = RequestMethod.POST)
	public ModelAndView curatorAddNewTrainer(
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam(value = "check", required = false) String check,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("forward:/curatorTrainerBodyPOST");
		Role role = new Role();
		role.setRoleId(3);
		role.setRoleType(RoleType.TRAINER);
		if ("true".equals(check)) {
			addUserByEmail(email, request, role);
		} else {
			addUserFromFile(file, request, role);
		}
		return model;
	}

	// Add single user by email if not exists
	private void addUserByEmail(String email, HttpServletRequest request,
			Role role) {
		User user = null;
		try {
			user = userService.findUser(email);
			if (user != null) {
				request.setAttribute("error", "This User already exists");
			} else {
				userService.addUser(email, role.getRoleType().getValue());
				request.setAttribute("succes",
						"New User has been added successfully");
			}
		} catch (DataException | NoSuchAlgorithmException e) {
			log.error("addUserByEmail", e);
		}
	}

	// upload xlsx file with email
	// need validation for xlsx file
	private void addUserFromFile(MultipartFile file,
			HttpServletRequest request, Role role) {
		String fileName = file.getOriginalFilename();
		if (fileName.endsWith(".xlsx")) {
			try {
				byte[] bytes;
				bytes = file.getBytes();
				InputStream is = new ByteArrayInputStream(bytes);
				List<String> emails = parser.parse(is);
				List<String> exEmails = userService.addListUser(emails, role
						.getRoleType().getValue());
				// add existing users into model
				List<String> notValid = UserValidator
						.getNotValidUserEmail(emails);
				request.setAttribute("exEmails", exEmails);
				request.setAttribute("notValid", notValid);

				if ((emails.size() - exEmails.size() - notValid.size()) > 0) {
					request.setAttribute("succes",
							"New User(s) from file has been added successfully.");
				}
			} catch (IOException | DataException | NoSuchAlgorithmException e) {
				log.error("addUserFromFile", e);
			}
		} else {
			request.setAttribute("error",
					"Please select a file with the extension xlsx.");
		}
	}

	@RequestMapping(value = "/curatorEditFormGroupLink", method = RequestMethod.GET)
	public ModelAndView curatorEditFormGroupLink(
			@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "groupId") int groupId) {
		ModelAndView model = new ModelAndView();
		Group group;
		try {
			group = groupService.findGroupById(groupId);

			model.addObject("cities", cityService.getAllCity());
			model.addObject("modules", moduleService.getAllModules());
			Role role = new Role();
			role.setRoleId(2);
			role.setRoleType(RoleType.MENTOR);
			model.addObject("group", group);
			model.addObject("mentors", userService.getAllUserByRole(role));
			model.addObject("vacations", group.getVacations());
			model.addObject("weekDuration",
					configService.getQuarterDuration());
			model.addObject("addressies", group.getAddress().getCity()
					.getAddresses());
		} catch (DataException e) {
			log.error("curatorEditFormGroupLink", e);
		}
		model.addObject("page", "../body/CuratorEditGroupBody.jsp");
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	@RequestMapping(value = "/curatorEditGroup", method = RequestMethod.POST)
	public ModelAndView curatorEditGroup(
			@RequestParam(value = "groupName", required = false) String groupName,
			@RequestParam(value = "menteeName", required = false) String menteeName,
			@RequestParam(value = "managerName", required = false) String managerName,
			@RequestParam(value = "mentorName", required = false) String mentorName,
			@RequestParam(value = "city", required = false) String cityName,
			@RequestParam(value = "street", required = false) String street,
			@RequestParam(value = "module", required = false) int module[],
			@RequestParam(value = "weekVocation", required = false) int[] weekVocation,
			@RequestParam(value = "groupId", required = false) int groupId,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("redirect:/curatorAddGroupLink");
		Address address;
		try {
			address = addressService.findAddress(street);

			User user = userService.findUser(mentorName);
			List<Vacation> vacations = null;
			List<Module> modules = null;
			if (weekVocation != null) {
				vacations = vacationService.getListVocationById(weekVocation);
			}
			if (module != null) {
				modules = moduleService.getModuleById(module);
			}
			Group group = groupService.findGroupById(groupId);
			group.setName(groupName);
			group.setManager(managerName);
			group.setMentee(menteeName);
			group.setModules(modules);
			group.setAddress(address);
			group.setVacations(vacations);
			group.setUser(user);
			groupService.updateGroup(group);
		} catch (DataException e) {
			log.error("curatorEditGroup", e);
		}
		return model;
	}

	// Link to Add Group Page by curator(admin)
	@RequestMapping(value = "/curatorAddGroupLink", method = RequestMethod.GET)
	public ModelAndView curatorAddGroupLink(
			@RequestParam(value = "page", required = false) String page,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		// Minsk city by default
		try {
			model.addObject("cities", cityService.getAllCity());

			model.addObject("modules", moduleService.getAllModules());
			Role role = new Role();
			role.setRoleId(2);
			role.setRoleType(RoleType.MENTOR);
			model.addObject("mentors", userService.getAllUserByRole(role));
			model.addObject("weekDuration",
					configService.getQuarterDuration());
			model.addObject("page", "../body/CuratorGroupBody.jsp");
			model.addObject("groups", groupService.getAllGroups());
		} catch (DataException e) {
			log.error("curatorAddGroupLink", e);
		}
		model.setViewName("curator/main/CuratorMainPage");
		return model;
	}

	// create new Group by curator
	@RequestMapping(value = "/curatorCreateGroup", method = RequestMethod.POST)
	public ModelAndView curatorCreateGroup(
			@RequestParam(value = "groupName", required = false) String groupName,
			@RequestParam(value = "menteeName", required = false) String menteeName,
			@RequestParam(value = "managerName", required = false) String managerName,
			@RequestParam(value = "mentorName", required = false) String mentorName,
			@RequestParam(value = "city", required = false) String cityName,
			@RequestParam(value = "street", required = false) String street,
			@RequestParam(value = "module", required = false) int module[],
			@RequestParam(value = "weekVocation", required = false) int[] weekVocation,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("redirect:/curatorAddGroupLink");
		Address address;
		try {
			address = addressService.findAddress(street);

			User user = userService.findUser(mentorName);
			List<Vacation> vacations = null;
			List<Module> modules = null;
			if (weekVocation != null) {
				vacations = vacationService.getListVocationById(weekVocation);
			}
			if (module != null) {
				modules = moduleService.getModuleById(module);
			}
			// adding new group
			groupService.addGroup(user, groupName, menteeName, managerName,
					address, modules, vacations);
		} catch (DataException e) {
			log.error("curatorCreateGroup", e);
		}
		// SWITCH MENTOR AND TRAINER
		return model;
	}

	// delete Group by ids
	@RequestMapping(value = "/curatorDeleteGroup", method = RequestMethod.POST)
	public ModelAndView curatorDeleteGroup(
			@RequestParam(value = "deleteGroup", required = false) int[] groupId,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("redirect:/curatorAddGroupLink");
		try {
			groupService.deleteGroupById(groupId);
		} catch (DataException e) {
			log.error("curatorDeleteGroup", e);
		}
		return model;
	}

	@RequestMapping(value = "/curatorCreateScheduleForWebinars", method = RequestMethod.GET)
	public ModelAndView curatorCreateScheduleForWebinars(
			HttpServletRequest request) {
		int weekNumberForEducation;
		ModelAndView model = null;
		try {
			weekNumberForEducation = configService
					.getQuarterDuration();

			int maxModuleDuration = 3;
			List<Group> groups = groupService.getAllGroups();
			List<Module> modules = moduleService.getAllModules();
			ScheduleVO scheduleVO = scheduleBuilder.createWebinarSchedule(
					groups, modules, weekNumberForEducation, maxModuleDuration);

			model = new ModelAndView("excelView", "scheduleVO", scheduleVO);
		} catch (DataException e) {
			log.error("curatorCreateScheduleForWebinars", e);
		}
		return model;
	}

	@RequestMapping(value = "/curatorCreateScheduleByLocation", method = RequestMethod.GET)

	public ModelAndView curatorCreateScheduleByLocation(
			HttpServletRequest request) {
		int weekNumberForEducation;
		ModelAndView model=null;
		try {
			weekNumberForEducation = configService
					.getQuarterDuration();
		
		int maxModuleDuration = 3;
		// this our finally schedule grid
		List<Group> groups = groupService.getAllGroups();
		List<Module> modules = moduleService.getAllModules();
		List<City> cities = cityService.getAllCity();
		Map<City, List<Group>> groupsByCityMap = new HashMap<City, List<Group>>();
		for (City city : cities) {
			List<Address> addressiesByCity = addressService
					.getAllAdressByCity(city);
			if (!addressiesByCity.isEmpty() && addressiesByCity != null) {
				List<Group> groupsByCity = new ArrayList<>();
				for (Address address : addressiesByCity) {
					List<Group> groupsByAddress = groupService
							.getAllGroupsByAddress(address);
					if (!groupsByAddress.isEmpty())
						groupsByCity.addAll(groupsByAddress);
				}

				if (!groupsByCity.isEmpty()) {
					groupsByCityMap.put(city, groupsByCity);
				}
			}
		}
		ScheduleVO scheduleVO = scheduleBuilder.createLocationSchedule(groups,
				modules, weekNumberForEducation, maxModuleDuration,
				groupsByCityMap);
		 model = new ModelAndView("excelView", "scheduleVO",
				scheduleVO);
		} catch (DataException e) {
			log.error("curatorCreateScheduleByLocation", e);
		}
		return model;
	}

	@RequestMapping(value = "/curatorCreateScheduleBySegment", method = RequestMethod.GET)
	public ModelAndView curatorCreateScheduleBySegment(
			HttpServletRequest request)  {
		ModelAndView model=null;
		try {
		int weekNumberForEducation = configService
				.getQuarterDuration();
		int maxModuleDuration = 3;
		List<Module> modules = moduleService.getAllModules();
		List<Group> groups = groupService.getAllGroups();
		List<Segment> segments = segmentService.getAllSegments();
		Map<Segment, List<Group>> groupsBySegmentMap = new HashMap<Segment, List<Group>>();
		for (Segment segment : segments) {
			List<Address> addressiesBySegment = addressService
					.getAllAdressBySegmentNumber(segment);
			if (!addressiesBySegment.isEmpty() && addressiesBySegment != null) {
				List<Group> groupsBySegment = new ArrayList<>();
				for (Address address : addressiesBySegment) {
					List<Group> groupsByAddress = groupService
							.getAllGroupsByAddress(address);
					if (!groupsByAddress.isEmpty()) {
						groupsBySegment.addAll(groupsByAddress);
					}
				}
				if (!groupsBySegment.isEmpty()) {
					groupsBySegmentMap.put(segment, groupsBySegment);
				}
			}
		}
		ScheduleVO scheduleVO = scheduleBuilder.createSegmentSchedule(groups,
				modules, weekNumberForEducation, maxModuleDuration,
				groupsBySegmentMap);
		 model = new ModelAndView("excelView", "scheduleVO",
				scheduleVO);
		} catch (DataException e) {
			log.error("curatorCreateScheduleBySegment", e);
		}
		return model;
	}
}