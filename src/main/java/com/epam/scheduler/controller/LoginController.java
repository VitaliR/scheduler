package com.epam.scheduler.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.logic.IGroupService;
import com.epam.scheduler.logic.IModuleService;
import com.epam.scheduler.logic.IUserService;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.User;

@Controller
public class LoginController {
	static Logger log = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private IUserService userService;
	@Autowired
	private ICityService cityService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IModuleService moduleService;
	@Autowired
	private IGroupService groupService;
	@Autowired
	private IConfigBaseDataOfScheduleService configService;

	@RequestMapping(value = { "/index", "/" }, method = RequestMethod.GET)
	public ModelAndView indext(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(Principal principal, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		if (principal != null) {
		if (AJAXController.period ||principal.getName().toLowerCase().equals("admin_admin@epam.com")) {
				User user;
				try {
					user = userService.findUser(principal.getName()
							.toLowerCase());

					model.addObject("user", user);
					request.getSession().setAttribute("user", user);
					Role role = user.getRole();
					switch (role.getRoleType()) {
					case ADMIN:
						model.addObject("page",
								"../body/CuratorScheduleBody.jsp");
						model.setViewName("curator/main/CuratorMainPage");
						break;
					case MENTOR:
						if (null == user.getFirstName()) {
							model = new ModelAndView(
									"redirect:/registerNewUserProfileLink");
						} else {
							model = new ModelAndView(
									"redirect:/mentorCreateGroupLink");
						}
						break;
					case TRAINER:
						if (null == user.getFirstName()) {
							model = new ModelAndView(
									"redirect:/registerNewUserProfileLink");
						} else {
							model.addObject("page",
									"/WEB-INF/jsp/trainer/body/TrainerScheduleBody.jsp");
							model.addObject("cities", cityService.getAllCity());
							model.setViewName("trainer/main/TrainerMainPage");
						}
						break;
					default:
						model.setViewName("login");
						break;
					}
				} catch (DataException e) {
					log.error("login", e);
				}
			} else {
				model.addObject("error",
						"Sorry, but the time of registration for groups left");
				model.setViewName("login");
			}
		} else {
			model.addObject("error", "Invalid User password or login");
			
			model.setViewName("login");
		}
		return model;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("succes", "You've been logged out successfully.");
		model.setViewName("login");
		return model;
	}

	// for 403 access denied page
	 @RequestMapping(value = "/403", method = RequestMethod.GET)
	 public ModelAndView accesssDenied(Principal user) {
		 ModelAndView model = new ModelAndView();
			if (user != null) {
				model.addObject("msg", "Hi <font color=\"red\">" + user.getName() 
				+ "</font>, you do not have permission to access this page!");
			} else {
				model.addObject("msg", 
				"You do not have permission to access this page!");
			}
			model.setViewName("403");
			return model;
	
	 }

}
