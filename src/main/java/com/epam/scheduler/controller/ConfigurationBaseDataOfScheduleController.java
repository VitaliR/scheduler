package com.epam.scheduler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.logic.IModuleService;
import com.epam.scheduler.logic.ISegmentService;

@Controller
public class ConfigurationBaseDataOfScheduleController {

	
	@Autowired
	private ICityService cityService;
	@Autowired
	private IConfigBaseDataOfScheduleService configService;
	@Autowired
	private IModuleService moduleService;
	@Autowired
	private ISegmentService segmentService;

	@RequestMapping(value = "/curatorConfigPage", method = RequestMethod.GET)
	public ModelAndView curatorMainPage(
			@RequestParam(value = "page", required = false) String page)
			throws DataException {
		ModelAndView model = new ModelAndView();
			if ("config".equalsIgnoreCase(page)) {
				model.addObject("page", "../body/CuratorConfigurationBody.jsp");
				model.addObject("cities", cityService.getAllCity());
				model.addObject("modules", moduleService.getAllModules());
				model.addObject("durationQuarter",
						configService.getQuarterDuration());
				model.addObject("segments", segmentService.getAllSegments());
				model.addObject("period", AJAXController.period);
			} else {
				model.addObject("page", "../body/CuratorConfigurationBody.jsp");
			}
		model.setViewName("curator/main/CuratorMainPage");
		 return model;
	}
}
