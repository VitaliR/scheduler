package com.epam.scheduler.exception;

public class DataException extends Exception {

	private static final long serialVersionUID = 3889621817306125769L;

	public DataException() {
		super();
	}

	public DataException(String msg, Throwable exc) {
		super(msg, exc);
	}

	public DataException(String msg) {
		super(msg);
	}

	public DataException(Throwable exc) {
		super(exc);
	}
}
