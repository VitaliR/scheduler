package com.epam.scheduler.parser.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.epam.scheduler.parser.IParser;

public class ParserImpl implements IParser {

	@Override
	public List<String> parse(InputStream is) throws IOException {
		List<String> listEmail = new LinkedList<>();
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(is);
		XSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					listEmail.add(cell.getStringCellValue().toLowerCase());
					break;
				default:
					break;
				}
			}
		}
		return listEmail;
	}
}
