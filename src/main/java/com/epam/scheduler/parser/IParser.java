package com.epam.scheduler.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface IParser {

    public List<String> parse(InputStream is) throws IOException;

}
