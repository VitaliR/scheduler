package com.epam.scheduler.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHAAlgorithm {

	     
	    public static String crypt(String passwordToHash) throws NoSuchAlgorithmException {
	        String securePassword = get_SHA_1_SecurePassword(passwordToHash);
	        return securePassword;
	    }
	 
	    private static String get_SHA_1_SecurePassword(String passwordToHash)
	    {
	        String generatedPassword = null;
	        try {
	            MessageDigest md = MessageDigest.getInstance("SHA-1");
	            byte[] bytes = md.digest(passwordToHash.getBytes());
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            generatedPassword = sb.toString();
	        }
	        catch (NoSuchAlgorithmException e)
	        {
	            e.printStackTrace();
	        }
	        return generatedPassword;
	    }
	     
//	    private static String get_SHA_256_SecurePassword(String passwordToHash, String salt)
//	    {
//	        //Use MessageDigest md = MessageDigest.getInstance("SHA-256");
//	    	return null;
//	    }
//	     
//	    private static String get_SHA_384_SecurePassword(String passwordToHash, String salt)
//	    {
//	        //Use MessageDigest md = MessageDigest.getInstance("SHA-384");
//	    	return null;
//	    }
//	     
//	    private static String get_SHA_512_SecurePassword(String passwordToHash, String salt)
//	    {
//	        //Use MessageDigest md = MessageDigest.getInstance("SHA-512");
//	    	return null;
//	    }
}