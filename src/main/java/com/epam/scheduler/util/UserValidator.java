package com.epam.scheduler.util;

import java.util.ArrayList;
import java.util.List;

public final class UserValidator {

	public static List<String> getNotValidUserEmail(List<String> emails) {
		List<String> notValid = new ArrayList<>();
		for (String email : emails) {
			if (!email
					.matches("^\\s*[a-zA-Z0-9]{3,30}\\_[a-zA-Z0-9]{3,34}@epam.com\\s*$")) {
				notValid.add(email);
			}
		}
		return notValid;
	}

}
