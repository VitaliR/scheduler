package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Segment;

public interface ISegmentService {
	public List<Segment> getAllSegments() throws DataException;
}
