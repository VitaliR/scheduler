package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Segment;

public interface IAddressService {

    public Address findAddress(String street) throws DataException;
    public List<Address> getAllAdressByCity(City city) throws DataException;
    public List<Address> getAllAdressBySegmentNumber(Segment segment) throws DataException;

}
