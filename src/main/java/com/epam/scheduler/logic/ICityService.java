package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.City;

public interface ICityService {

    public City findCity(String name) throws DataException;
    public City findCityById(int id) throws DataException;
    public List<City> getAllCity() throws DataException;
    
}
