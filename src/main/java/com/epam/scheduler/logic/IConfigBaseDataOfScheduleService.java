package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.Module;
public interface IConfigBaseDataOfScheduleService {
	
	public int getQuarterDuration () throws DataException;
	public int modificationVacation(int durationId) throws DataException;
	public boolean deleteOfficeByStreet(String street) throws DataException;
	public boolean addNewOffice(Address address)throws DataException;
	public Module changeModule(Module module) throws DataException;
	public boolean deleteModuleByListId(int[] moduleId) throws DataException;
	public int addModule(Module moduleId)throws DataException;
	public boolean deleteAllMentors()throws DataException;
	public void deleteAllGroups() throws DataException;
	public List<String> listDeleteMentors(String[] mentorList) throws DataException;
	public List<String> deleteTrainerList(String[] trainerList) throws DataException;
}
