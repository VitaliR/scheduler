package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Module;

public interface IModuleService {
	public List<Module> getAllModules() throws DataException;

	public List<Module> getModuleById(int moduleId[]) throws DataException;

	public Module findModuleById(int moduleId) throws DataException;
}
