package com.epam.scheduler.logic;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.User;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public interface IUserService {

    public User findUser(String email) throws DataException;
//    public City findCity(String name) throws DataException;
    public void addUser(String email, String roleName) throws DataException, NoSuchAlgorithmException;
    public void updateUser(User user) throws DataException;
    public List<User> getAllUserByRole(Role role) throws DataException;
    /**
     *
     * @param is - an input stream
     * @param roleName - a role of user
     * @return the list of existing emails
     * @throws DataException
     * @throws IOException
     * @throws NoSuchAlgorithmException 
     */
  public List<String>  addListUser(List<String> emails, String roleName) throws DataException,IOException, NoSuchAlgorithmException;

}
