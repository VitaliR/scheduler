package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Vacation;

public interface IVacationService {
	public List<Vacation> getListVocationById(int vocation[]) throws DataException; 
	public List<Vacation> getAllListVacation()throws DataException;
}
