package com.epam.scheduler.logic;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.WeekScheduleStatus;

public interface ISchedulerService {
	
	public Map<Integer, List<Integer>> fillSelfStudyModules(
			Map<Integer, List<Integer>> grid, Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			Map<Integer, List<Integer>> selfStudy, int moduleDuration,
			int weekNumberForAducation);
	
	public Map<Integer, List<Integer>> createSchedule(
			Map<Integer, List<Integer>> grid,Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration,
			int moduleDuration, int weekNumberForAducation,boolean location);

	public void fillVacationInSchedulerGrid(Map<Integer, List<Integer>> grid,
			List<Group> groups, Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus);

	public Map<Integer, List<WeekScheduleStatus>> createSchedulerGridWeekStatus(List<Group> groups,
			int weekNumberForAducation) throws DataException;
	
	public Map<Integer, List<Integer>> createSchedulerGrid(
			List<Group> groups, int weekNumberForAducation)
			throws DataException;

	public TreeMap<Integer, List<Integer>> sortGroupsByModulesWithDuration(
			List<Group> groups, List<Module> modules, int durationForModule)
			throws DataException;
	public TreeMap<Integer, List<Integer>> sortByGroups(Map<Integer, List<Integer>> map);
}
