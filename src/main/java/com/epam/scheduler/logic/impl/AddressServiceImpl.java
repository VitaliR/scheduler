package com.epam.scheduler.logic.impl;

import java.util.List;

import com.epam.scheduler.dao.IAddressDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Segment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
@Transactional
public class AddressServiceImpl implements IAddressService {

    @Autowired
    private IAddressDAO addressDAO;

    @Override
    public Address findAddress(String street) throws DataException {
        return addressDAO.findAddress(street);
    }

	@Override
	public List<Address> getAllAdressByCity(City city) throws DataException {
		return addressDAO.getAllAdressByCity(city);
	}

	@Override
	public List<Address> getAllAdressBySegmentNumber(Segment segment)
			throws DataException {
		return addressDAO.getAllAdressBySegmentNumber(segment);
		
	}
	
    
}
