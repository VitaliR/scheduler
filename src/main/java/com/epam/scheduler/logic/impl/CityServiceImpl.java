package com.epam.scheduler.logic.impl;

import java.util.List;

import com.epam.scheduler.dao.ICityDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.model.City;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
@Transactional
public class CityServiceImpl implements ICityService {

    @Autowired
    private ICityDAO cityDAO;

   
    @Override
    public City findCity(String name) throws DataException {
        return cityDAO.findCity(name);
    }
	@Override
	public List<City> getAllCity() throws DataException {
		return cityDAO.getAllCity();
	}
	@Override
	public City findCityById(int id) throws DataException {
		return cityDAO.findCityById(id);
	}
    
    
}
