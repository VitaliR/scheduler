package com.epam.scheduler.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.scheduler.dao.IVacationDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IVacationService;
import com.epam.scheduler.model.Vacation;
@Transactional
public class VacationServiceImp implements IVacationService{

	@Autowired
	private IVacationDAO vacationDAO;
	
	@Override
	public List<Vacation> getListVocationById(int[] vacation)
			throws DataException {
		return vacationDAO.getListVocationById(vacation);
	}

	@Override
	public List<Vacation> getAllListVacation() throws DataException {
		return vacationDAO.getAllListVacation();
	}
	

}
