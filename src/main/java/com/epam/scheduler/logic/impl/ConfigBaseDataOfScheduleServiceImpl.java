package com.epam.scheduler.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.scheduler.dao.IConfigBaseDataOfScheduleDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IAddressService;
import com.epam.scheduler.logic.ICityService;
import com.epam.scheduler.logic.IConfigBaseDataOfScheduleService;
import com.epam.scheduler.logic.IGroupService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;

@Service
@Transactional
public class ConfigBaseDataOfScheduleServiceImpl implements
		IConfigBaseDataOfScheduleService {
	@Autowired
	private IConfigBaseDataOfScheduleDAO configDao;
	@Autowired
	private ICityService cityService;
	@Autowired
	private IAddressService addressService;
	@Autowired
	private IGroupService groupService;

	@Override
	public int getQuarterDuration() throws DataException {
		return configDao.getQuarterDuration();
	}

	@Override
	public int modificationVacation(int durationId) throws DataException {
		if (configDao.isEmptyGroupTable()) {
			configDao.modificationVacation(durationId);
			return configDao.getQuarterDuration();
			
		} else {
			return 0;
		}
	}

	@Override
	public boolean deleteOfficeByStreet(String street) throws DataException {

		List<Group> listGroup = groupService
				.getAllGroupsByAddress(addressService.findAddress(street));

		int userCount = configDao.getUserByAddressId(addressService
				.findAddress(street));
		if (0 == listGroup.size() && 0 == userCount) {
			configDao.deleteOfficeByStreet(street);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean addNewOffice(Address address) throws DataException {
		if (configDao.isThereOffice(address.getStreet())) {
			return false;
		} else {
			City city = cityService.findCityById(address.getCity().getCityId());
			address.setCity(city);
			configDao.addNewOffice(address);
		}
		return true;
	}

	@Override
	public Module changeModule(Module module) throws DataException {
		return configDao.changeModule(module);
	}

	@Override
	public boolean deleteModuleByListId(int[] moduleId) throws DataException {
		if (configDao.isEmptyGroupTable()) {
			configDao.deleteModuleByListId(moduleId);
			return true;
		}
		return false;
	}

	@Override
	public int addModule(Module module) throws DataException {
		return configDao.addModule(module);
	}

	@Override
	public boolean deleteAllMentors() throws DataException {
		if (configDao.isEmptyGroupTable()) {
			configDao.deleteAllMentors();
			return true;
		}
		return false;
	}

	@Override
	public void deleteAllGroups() throws DataException {
		configDao.deleteAllGroups();
	}

	@Override
	public List<String> listDeleteMentors(String[] mentorList)
			throws DataException {
		return configDao.listDeleteMentors(mentorList);
	}

	@Override
	public List<String> deleteTrainerList(String[] trainerList)
			throws DataException {
		return configDao.deleteTrainerList(trainerList);
	}

}
