package com.epam.scheduler.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.scheduler.dao.IGroupDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IGroupService;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.User;
import com.epam.scheduler.model.Vacation;
@Transactional
public class GroupServiceImpl implements IGroupService {

    @Autowired
    private IGroupDAO groupDAO;

   
    @Override
    public void addGroup(User user, String groupName, String menteeName, String managerName,
			Address address, List<Module> modules, List<Vacation> vacations) throws DataException{
        groupDAO.addGroup(user, groupName, menteeName, managerName, address, modules, vacations);
    }
    
	@Override
	public List<Group> getAllGroupsByUser(User user) throws DataException {
		return groupDAO.getAllGroupsByUser(user);
	}

	@Override
	public List<Group> getAllGroups() throws DataException {
		return groupDAO.getAllGroups();
	}

	@Override
	public void deleteGroupById(int[] groupIds) throws DataException {
		groupDAO.deleteGroupById(groupIds);
		
	}

	@Override
	public Group findGroupById(int groupId) throws DataException {
		return groupDAO.findGroupById(groupId);
	}

	@Override
	public List<Group> getAllGroupsByCity(City city) throws DataException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Group> getAllGroupsByAddress(Address address)
			throws DataException {
		return groupDAO.getAllGroupsByAddress(address);
	}

	@Override
	public void updateGroup(Group group) throws DataException {
		groupDAO.updateGroup(group);
		
	}
}
