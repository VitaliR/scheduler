package com.epam.scheduler.logic.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import scheduler.IScheduler;
import scheduler.impl.SchedulerImpl;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.ISchedulerService;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.WeekScheduleStatus;

public class SchedulerServiceImpl implements ISchedulerService {
	private IScheduler scheduler = new SchedulerImpl();

	@Override
	public Map<Integer, List<Integer>> createSchedule(
			Map<Integer, List<Integer>> grid,Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration,
			int moduleDuration, int weekNumberForAducation,boolean location){
		return scheduler.createSchedule(grid, gridForScheduleWeekStatus, allModulesIdsWithGroupListWithDuration, moduleDuration, weekNumberForAducation, location);
	}

	@Override
	public void fillVacationInSchedulerGrid(Map<Integer, List<Integer>> grid,
			List<Group> groups ,Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus) {
		scheduler.fillVacationInSchedulerGrid(grid, groups,gridForScheduleWeekStatus);

	}

	public Map<Integer, List<WeekScheduleStatus>> createSchedulerGridWeekStatus(List<Group> groups,
			int weekNumberForAducation) throws DataException{
	return scheduler.createSchedulerGridWeekStatus(groups, weekNumberForAducation);
	}
	
	@Override
	public Map<Integer, List<Integer>> createSchedulerGrid(List<Group> groups,
			int weekNumberForAducation) throws DataException {
		return scheduler.createSchedulerGrid(groups, weekNumberForAducation);
	}

	@Override
	public TreeMap<Integer, List<Integer>> sortGroupsByModulesWithDuration(
			List<Group> groups, List<Module> modules, int durationForModule)
			throws DataException {
		return scheduler.sortGroupsByModulesWithDuration(groups,
				modules, durationForModule);
	}

	@Override
	public Map<Integer, List<Integer>> fillSelfStudyModules(
			Map<Integer, List<Integer>> grid, Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus,
			Map<Integer, List<Integer>> selfStudy, int moduleDuration,
			int weekNumberForAducation) {
		
		return scheduler.fillSelfStudyModules(grid, gridForScheduleWeekStatus, selfStudy, moduleDuration, weekNumberForAducation);
	}

	@Override
	public TreeMap<Integer, List<Integer>> sortByGroups(
			Map<Integer, List<Integer>> map) {
		
		return scheduler.sortByGroups(map);
	}

}
