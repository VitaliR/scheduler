package com.epam.scheduler.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.scheduler.dao.ISegmentDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.ISegmentService;
import com.epam.scheduler.model.Segment;

@Transactional
public class SegmentServiceImpl implements ISegmentService {

	@Autowired
	private ISegmentDAO segmentDAO;

	@Override
	public List<Segment> getAllSegments() throws DataException {
		return segmentDAO.getAllSegments();
	}

	

}
