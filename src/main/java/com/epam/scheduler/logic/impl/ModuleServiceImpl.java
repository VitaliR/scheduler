package com.epam.scheduler.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.scheduler.dao.IModuleDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IModuleService;
import com.epam.scheduler.model.Module;
@Transactional
public class ModuleServiceImpl implements IModuleService {

	@Autowired
	private IModuleDAO moduleDAO;

	@Override
	public List<Module> getAllModules() throws DataException {
		return moduleDAO.getAllModules();
	}


	@Override
	public List<Module> getModuleById(int[] moduleId) throws DataException {
		return moduleDAO.getModuleById(moduleId);
	}


	@Override
	public Module findModuleById(int moduleId) throws DataException {
		return moduleDAO.findModuleById(moduleId);
	}

}
