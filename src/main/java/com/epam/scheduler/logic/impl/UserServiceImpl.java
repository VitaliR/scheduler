package com.epam.scheduler.logic.impl;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.scheduler.dao.IUserDAO;
import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.IUserService;
import com.epam.scheduler.model.Role;
import com.epam.scheduler.model.User;
import com.epam.scheduler.util.UserValidator;

@Transactional
@Service(value = "userDetailsService")
public class UserServiceImpl implements IUserService, UserDetailsService {

	@Autowired
	private IUserDAO userDAO;

	@Override
	public User findUser(String email) throws DataException {
		return userDAO.findUser(email);
	}

	@Override
	public void updateUser(User user) throws DataException {
		userDAO.updateUser(user);
	}

	@Override
	public void addUser(String email, String roleName) throws DataException,
			NoSuchAlgorithmException {
		userDAO.addUser(email, roleName);
	}

	@Override
	public List<String> addListUser(List<String> emails, String roleName)
			throws DataException, IOException, NoSuchAlgorithmException {
		List<String> emailsCopyList = new ArrayList<>(emails);
		List<String> exEmails = userDAO.findExistingEmails(emailsCopyList);
		List<String> notValidEmail = UserValidator
				.getNotValidUserEmail(emailsCopyList);
		emailsCopyList.removeAll(exEmails);
		emailsCopyList.removeAll(notValidEmail);
		userDAO.addListUser(emailsCopyList, roleName);
		exEmails.remove(notValidEmail);
		return exEmails;
	}

	@Override
	public List<User> getAllUserByRole(Role role) throws DataException {
		return userDAO.getAllUserByRole(role);
	}

	@Override
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException {

		User details;
		try {
			details = userDAO.findUser(email.toLowerCase());

			if (details != null) {
				Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
				SimpleGrantedAuthority mentorAuthority = new SimpleGrantedAuthority(
						"ROLE_MENTOR");
				SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority(
						"ROLE_ADMIN");
				if (details.getRole().getRoleType().getValue().equals("mentor")) {
					authorities.add(mentorAuthority);
				} else if (details.getRole().getRoleType().getValue()
						.equals("admin")) {
					authorities.add(adminAuthority);
				}
				UserDetails user = new org.springframework.security.core.userdetails.User(
						details.getEmail(), details.getPassword(), true, true,
						true, true, authorities);
				return user;
			}else {
				throw new UsernameNotFoundException("User not found");
			}
		} catch (DataException e) {
			throw new UsernameNotFoundException("User not found");
		}
	}
}
