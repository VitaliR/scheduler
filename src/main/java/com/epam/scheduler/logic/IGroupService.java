package com.epam.scheduler.logic;

import java.util.List;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Address;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.User;
import com.epam.scheduler.model.Vacation;

public interface IGroupService {
	public void addGroup(User user, String groupName, String menteeName,String managerName,
			Address address, List<Module> modules, List<Vacation> vacations) throws DataException;

	public List<Group> getAllGroupsByUser(User user) throws DataException;
	public List<Group> getAllGroups() throws DataException;
	public List<Group> getAllGroupsByCity(City city) throws DataException;
	public List<Group> getAllGroupsByAddress(Address address) throws DataException;
	public void deleteGroupById(int[] groupIds) throws DataException;
	public Group findGroupById(int groupId) throws DataException;
	public void updateGroup(Group group) throws DataException;
}
