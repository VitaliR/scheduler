package com.epam.scheduler.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "segment")
public class Segment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1582955695805471719L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "segment_seq")
	@SequenceGenerator(name = "segment_seq", sequenceName = "segment_seq", allocationSize = 1)
	@Column(name = "segment_id", unique = true, nullable = false)
	private int segmentId;

	@Column(name = "segmentNumber", unique = true, nullable = false)
	private int segmentNumber;

	public Segment() {
	}

	public int getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(int segmentId) {
		this.segmentId = segmentId;
	}

	public int getSegmentNumber() {
		return segmentNumber;
	}

	public void setSegmentNumber(int segmentNumber) {
		this.segmentNumber = segmentNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + segmentId;
		result = prime * result + segmentNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Segment other = (Segment) obj;
		if (segmentId != other.segmentId)
			return false;
		if (segmentNumber != other.segmentNumber)
			return false;
		return true;
	}

}
