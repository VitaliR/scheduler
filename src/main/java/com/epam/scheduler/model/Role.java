package com.epam.scheduler.model;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "role")
public class Role implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9070771662660118455L;

	@Id
	@Column(name = "role_id", unique = true, nullable = false)
	private int roleId;

	@Enumerated(EnumType.STRING)
	@Column(name = "role_name", unique = true, nullable = false)
	private RoleType roleType;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "role")
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	private Set<User> users = new HashSet<>();

	public Role() {
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + roleId;
		result = prime * result
				+ ((roleType == null) ? 0 : roleType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (roleId != other.roleId)
			return false;
		if (roleType != other.roleType)
			return false;
		return true;
	}

}
