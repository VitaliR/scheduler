package com.epam.scheduler.model;

public enum RoleType {
    ADMIN("admin"), MENTOR("mentor"), TRAINER("trainer");

    private String value;

    private RoleType(String value) {
        this.value = value;
    }
    /**
     * @return the name of this enum constant by string value
     */
    public String getValue() {
        return value;
    }
    
}

