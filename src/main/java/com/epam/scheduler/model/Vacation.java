package com.epam.scheduler.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vacation")
public class Vacation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3528645099944069673L;

	@Id
    @Column(name = "vacation_id", unique = true,nullable = true)
	private int id;
	
	@Column(name = "week_number", unique = true, nullable = true)
	private int weekNumber;
	
	public Vacation() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacation other = (Vacation) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
