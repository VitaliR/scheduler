package com.epam.scheduler.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "\"group\"")
public class Group implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4264073976763188481L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_seq")
	@SequenceGenerator(name = "group_seq", sequenceName = "group_seq", allocationSize = 1)
	@Column(name = "group_id", unique = true, nullable = false)
	private int groupId;

	@Column(name = "group_name", length = 50)
	private String name;

	@ManyToOne
	@JoinColumn(name = "user_email")
	private User user;

	@Column(name = "mentee", length = 74)
	private String mentee;

	@Column(name = "manager", length = 74)
	private String manager;

	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	@JoinTable(name = "group_module", joinColumns = { @JoinColumn(name = "group_id") }, inverseJoinColumns = { @JoinColumn(name = "module_id") })
	private List<Module> modules = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "address_id")
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	private Address address;

	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	@JoinTable(name = "group_vacation", joinColumns = { @JoinColumn(name = "group_id") }, inverseJoinColumns = { @JoinColumn(name = "vacation_id") })
	private List<Vacation> vacations = new ArrayList<>();

	public Group() {
	}

	public Group(String name, User user, String mentee, String manager,
			Address address) {
		super();
		this.name = name;
		this.user = user;
		this.mentee = mentee;
		this.manager = manager;
		this.address = address;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMentee() {
		return mentee;
	}

	public void setMentee(String mentee) {
		this.mentee = mentee;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Vacation> getVacations() {
		return vacations;
	}

	public void setVacations(List<Vacation> vacations) {
		this.vacations = vacations;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + groupId;
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result + ((mentee == null) ? 0 : mentee.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (groupId != other.groupId)
			return false;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (mentee == null) {
			if (other.mentee != null)
				return false;
		} else if (!mentee.equals(other.mentee))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Group [groupId=" + groupId + ", name=" + name + ", modules="
				+ modules + "]";
	}

}