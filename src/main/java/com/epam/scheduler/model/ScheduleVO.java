package com.epam.scheduler.model;

import java.util.List;
import java.util.Map;

public class ScheduleVO {
	private int weekNumberEducation;
	private Map<Integer, List<Integer>> grid;
	private Map<Integer, Map<Integer, List<Integer>>> notIncludedModulesInSchedule;
	private Map<Integer, Module> modulesMap;
	private Map<Integer, Group> groupsMap;
	private Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus;

	public int getWeekNumberEducation() {
		return weekNumberEducation;
	}

	public void setWeekNumberEducation(int weekNumberEducation) {
		this.weekNumberEducation = weekNumberEducation;
	}

	public Map<Integer, List<WeekScheduleStatus>> getGridForScheduleWeekStatus() {
		return gridForScheduleWeekStatus;
	}

	public void setGridForScheduleWeekStatus(
			Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus) {
		this.gridForScheduleWeekStatus = gridForScheduleWeekStatus;
	}

	public Map<Integer, List<Integer>> getGrid() {
		return grid;
	}

	public void setGrid(Map<Integer, List<Integer>> grid) {
		this.grid = grid;
	}

	public Map<Integer, Map<Integer, List<Integer>>> getNotIncludedModulesInSchedule() {
		return notIncludedModulesInSchedule;
	}

	public void setNotIncludedModulesInSchedule(
			Map<Integer, Map<Integer, List<Integer>>> notIncludedModulesInSchedule) {
		this.notIncludedModulesInSchedule = notIncludedModulesInSchedule;
	}

	public Map<Integer, Module> getModulesMap() {
		return modulesMap;
	}

	public void setModulesMap(Map<Integer, Module> modulesMap) {
		this.modulesMap = modulesMap;
	}

	public Map<Integer, Group> getGroupsMap() {
		return groupsMap;
	}

	public void setGroupsMap(Map<Integer, Group> groupsMap) {
		this.groupsMap = groupsMap;
	}

}
