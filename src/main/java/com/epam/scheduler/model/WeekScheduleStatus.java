package com.epam.scheduler.model;

public enum WeekScheduleStatus {
	WEBINAR("webinar"), LECTURE("lecture"), SELFSTUDY("selfstudy"),VACATION("vacation"),FREE("free");

    private String status;

    private WeekScheduleStatus(String value) {
        this.status = value;
    }
    /**
     * @return the name of this enum constant by string value
     */
    public String getValue() {
        return status;
    }
}
