package com.epam.scheduler.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2341430772598129516L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq")
	@SequenceGenerator(name = "address_seq", sequenceName = "address_seq", allocationSize = 1)
	@Column(name = "address_id", unique = true, nullable = false)
	private int addressId;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	@JoinColumn(name = "city_id")
	private City city;
	// validate data
	@Size(min = 3, max = 50, message = "length.configform.address.name")
	@Pattern(regexp = "^[\\s\\w\\s,-]+$")
	@Column(name = "address_name", length = 50)
	private String street;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	private Set<User> users = new HashSet<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ org.hibernate.annotations.CascadeType.DETACH })
	@JoinColumn(name = "segment_id")
	private Segment segment;

	public Address() {
	}

	public Address(City city, String street) {
		this.city = city;
		this.street = street;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + addressId;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((segment == null) ? 0 : segment.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (addressId != other.addressId)
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (segment == null) {
			if (other.segment != null)
				return false;
		} else if (!segment.equals(other.segment))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		return true;
	}

}
