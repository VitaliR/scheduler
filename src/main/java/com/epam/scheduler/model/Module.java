package com.epam.scheduler.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.io.Serializable;

@Entity
@Table(name = "module")
public class Module implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8838266955884954854L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "module_seq")
	@SequenceGenerator(name = "module_seq", sequenceName = "module_seq", allocationSize = 1)
	@Column(name = "module_id", unique = true, nullable = false)
	private Integer moduleId;
	// validate data
	@NotNull
	@Size(min = 3, max = 70, message = "length.configform.module.name")
	@Pattern(regexp = "^[\\s\\w\\(\\)-]+$")
	@Column(name = "module_name", length = 70)
	private String name;
	// validate data
	@NotNull
	@Min(1)
	@Max(5)
	@Column(name = "module_duration")
	private Integer duration;

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((duration == null) ? 0 : duration.hashCode());
		result = prime * result
				+ ((moduleId == null) ? 0 : moduleId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Module other = (Module) obj;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (moduleId == null) {
			if (other.moduleId != null)
				return false;
		} else if (!moduleId.equals(other.moduleId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Module [moduleId=" + moduleId + ", name=" + name
				+ ", duration=" + duration + "]";
	}

}
