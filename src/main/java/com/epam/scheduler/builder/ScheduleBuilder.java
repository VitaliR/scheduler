package com.epam.scheduler.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.logic.ISchedulerService;
import com.epam.scheduler.model.City;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.ScheduleVO;
import com.epam.scheduler.model.Segment;
import com.epam.scheduler.model.WeekScheduleStatus;

public class ScheduleBuilder {

	@Autowired
	private ISchedulerService schedulerService;

	public ScheduleVO createWebinarSchedule(List<Group> groups,
			List<Module> modules, int weekNumberForEducation,
			int maxModuleDuration) throws DataException {
		ScheduleVO scheduleVO = new ScheduleVO();
		// map for sorted modules by groups in each module duration
		Map<Integer, TreeMap<Integer, List<Integer>>> sortedModeuleByGroups = new HashMap<>();
		// sort modules
		for (int i = 1; i <= maxModuleDuration; i++) {
			TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration = schedulerService
					.sortGroupsByModulesWithDuration(groups, modules, i);
			sortedModeuleByGroups
					.put(i, allModulesIdsWithGroupListWithDuration);
		}
		// create schedule grid
		Map<Integer, List<Integer>> grid = schedulerService
				.createSchedulerGrid(groups, weekNumberForEducation);
		// create WeekStatusGrid
		Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus = schedulerService
				.createSchedulerGridWeekStatus(groups, weekNumberForEducation);
		// fill vacation in grids
		schedulerService.fillVacationInSchedulerGrid(grid, groups,
				gridForScheduleWeekStatus);
		// map for selfstudy modules by groups in each module duration
		Map<Integer, Map<Integer, List<Integer>>> selfStudyModulesByGroups = new HashMap<>();
		for (int i = maxModuleDuration; i >= 1; i--) {
			Map<Integer, List<Integer>> selfStudyAllModulesIdsWithGroupListWithDuration = schedulerService
					.createSchedule(grid, gridForScheduleWeekStatus,
							sortedModeuleByGroups.get(i), i,
							weekNumberForEducation, false);
			selfStudyModulesByGroups.put(i,
					selfStudyAllModulesIdsWithGroupListWithDuration);
		}
		// map for not included modules by groups in each module duration
		Map<Integer, Map<Integer, List<Integer>>> notIncludedModulesInSchedule = new HashMap<>();
		for (int i = maxModuleDuration; i >= 1; i--) {
			Map<Integer, List<Integer>> notIncludedByDuration = schedulerService
					.fillSelfStudyModules(grid, gridForScheduleWeekStatus,
							selfStudyModulesByGroups.get(i), i,
							weekNumberForEducation);
			notIncludedModulesInSchedule.put(i, notIncludedByDuration);
		}
		// groups MAP
		Map<Integer, Group> groupsMap = new HashMap<>();
		for (Group group : groups) {
			groupsMap.put(group.getGroupId(), group);
		}
		// modules MAP
		Map<Integer, Module> modulesMap = new HashMap<>();
		for (Module module : modules) {
			modulesMap.put(module.getModuleId(), module);
		}
		scheduleVO.setWeekNumberEducation(weekNumberForEducation);
		scheduleVO.setGridForScheduleWeekStatus(gridForScheduleWeekStatus);
		scheduleVO.setGroupsMap(groupsMap);
		scheduleVO.setModulesMap(modulesMap);
		scheduleVO.setGrid(grid);
		scheduleVO
				.setNotIncludedModulesInSchedule(notIncludedModulesInSchedule);
		return scheduleVO;
	}


	public ScheduleVO createSegmentSchedule(List<Group> groups,
			List<Module> modules, int weekNumberForEducation,
			int maxModuleDuration, Map<Segment, List<Group>> groupsBySegmentMap)
			throws DataException {
		ScheduleVO scheduleVO = new ScheduleVO();

		// create schedule grid
		Map<Integer, List<Integer>> grid = schedulerService
				.createSchedulerGrid(groups, weekNumberForEducation);

		// create WeekStatusGrid
		Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus = schedulerService
				.createSchedulerGridWeekStatus(groups, weekNumberForEducation);

		Map<Integer, Map<Integer, List<Integer>>> webinars = new HashMap<>();
		for (int i = 1; i <= weekNumberForEducation; i++) {
			webinars.put(i, new HashMap<Integer, List<Integer>>());
		}
		// for each groups sorted by segment (addresses)
		for (Map.Entry<Segment, List<Group>> entry : groupsBySegmentMap
				.entrySet()) {
			// 1.
			List<Group> groupsBySegment = entry.getValue();
			// map for sorted modules by groups in each module duration
			Map<Integer, TreeMap<Integer, List<Integer>>> sortedModulesByGroups = new HashMap<>();
			for (int i = 1; i <= maxModuleDuration; i++) {
				TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration = schedulerService
						.sortGroupsByModulesWithDuration(groupsBySegment,
								modules, i);
				sortedModulesByGroups.put(i,
						allModulesIdsWithGroupListWithDuration);
			}
			// 2.
			// fill vacation in grids
			schedulerService.fillVacationInSchedulerGrid(grid, groupsBySegment,
					gridForScheduleWeekStatus);
			// map for webinar modules by groups in each module duration
			// 3.

			for (int i = maxModuleDuration; i >= 1; i--) {
				Map<Integer, List<Integer>> selfStudyAllModulesIdsWithGroupListWithDuration = schedulerService
						.createSchedule(grid, gridForScheduleWeekStatus,
								sortedModulesByGroups.get(i), i,
								weekNumberForEducation, true);
				Map<Integer, List<Integer>> webinarByDuration = webinars.get(i);
				putAllModulesForWebinars(webinarByDuration,
						selfStudyAllModulesIdsWithGroupListWithDuration);
			}
			// 4.

			// sort modules
		}
		Map<Integer, TreeMap<Integer, List<Integer>>> sortedModuleByGroups = new HashMap<>();
		// sort modules
		for (int i = 1; i <= maxModuleDuration; i++) {
			TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration = schedulerService
					.sortByGroups(webinars.get(i));
			sortedModuleByGroups.put(i, allModulesIdsWithGroupListWithDuration);
		}
		// map for selfstudy modules by groups in each module duration
		Map<Integer, Map<Integer, List<Integer>>> selfStudyModulesByGroups = new HashMap<>();
		for (int i = maxModuleDuration; i >= 1; i--) {
			Map<Integer, List<Integer>> selfStudyAllModulesIdsWithGroupListWithDuration = schedulerService
					.createSchedule(grid, gridForScheduleWeekStatus,
							sortedModuleByGroups.get(i), i,
							weekNumberForEducation, false);
			selfStudyModulesByGroups.put(i,
					selfStudyAllModulesIdsWithGroupListWithDuration);
		}
		// map for not included modules by groups in each module duration
		Map<Integer, Map<Integer, List<Integer>>> notIncludedModulesInSchedule = new HashMap<>();
		for (int i = maxModuleDuration; i >= 1; i--) {
			Map<Integer, List<Integer>> notIncludedByDuration = schedulerService
					.fillSelfStudyModules(grid, gridForScheduleWeekStatus,
							selfStudyModulesByGroups.get(i), i,
							weekNumberForEducation);
			notIncludedModulesInSchedule.put(i, notIncludedByDuration);
		}
		// groups MAP
		Map<Integer, Group> groupsMap = new HashMap<>();
		for (Group group : groups) {
			groupsMap.put(group.getGroupId(), group);
		}
		// modules MAP
		Map<Integer, Module> modulesMap = new HashMap<>();
		for (Module module : modules) {
			modulesMap.put(module.getModuleId(), module);
		}

		scheduleVO.setWeekNumberEducation(weekNumberForEducation);
		scheduleVO.setGridForScheduleWeekStatus(gridForScheduleWeekStatus);
		scheduleVO.setGroupsMap(groupsMap);
		scheduleVO.setModulesMap(modulesMap);
		scheduleVO.setGrid(grid);
		scheduleVO
				.setNotIncludedModulesInSchedule(notIncludedModulesInSchedule);
		return scheduleVO;
	}

	
	public ScheduleVO createLocationSchedule(List<Group> groups,
			List<Module> modules, int weekNumberForEducation,
			int maxModuleDuration, Map<City, List<Group>> groupsByCityMap)
			throws DataException {
		ScheduleVO scheduleVO = new ScheduleVO();

		// create schedule grid
		Map<Integer, List<Integer>> grid = schedulerService
				.createSchedulerGrid(groups, weekNumberForEducation);

		// create WeekStatusGrid
		Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus = schedulerService
				.createSchedulerGridWeekStatus(groups, weekNumberForEducation);

		Map<Integer, Map<Integer, List<Integer>>> webinars = new HashMap<>();
		for (int i = 1; i <= weekNumberForEducation; i++) {
			webinars.put(i, new HashMap<Integer, List<Integer>>());
		}
		// for each groups sorted by segment (addresses)
		for (Map.Entry<City, List<Group>> entry : groupsByCityMap
				.entrySet()) {
			// 1.
			List<Group> groupsByCity = entry.getValue();
			// map for sorted modules by groups in each module duration
			Map<Integer, TreeMap<Integer, List<Integer>>> sortedModulesByGroups = new HashMap<>();
			for (int i = 1; i <= maxModuleDuration; i++) {
				TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration = schedulerService
						.sortGroupsByModulesWithDuration(groupsByCity,
								modules, i);
				sortedModulesByGroups.put(i,
						allModulesIdsWithGroupListWithDuration);
			}
			// 2.
			// fill vacation in grids
			schedulerService.fillVacationInSchedulerGrid(grid, groupsByCity,
					gridForScheduleWeekStatus);
			// map for webinar modules by groups in each module duration

			for (int i = maxModuleDuration; i >= 1; i--) {
				Map<Integer, List<Integer>> selfStudyAllModulesIdsWithGroupListWithDuration = schedulerService
						.createSchedule(grid, gridForScheduleWeekStatus,
								sortedModulesByGroups.get(i), i,
								weekNumberForEducation, true);
				Map<Integer, List<Integer>> webinarByDuration = webinars.get(i);
				putAllModulesForWebinars(webinarByDuration,
						selfStudyAllModulesIdsWithGroupListWithDuration);
			}
		}
		Map<Integer, TreeMap<Integer, List<Integer>>> sortedModuleByGroups = new HashMap<>();
		// sort modules
		for (int i = 1; i <= maxModuleDuration; i++) {
			TreeMap<Integer, List<Integer>> allModulesIdsWithGroupListWithDuration = schedulerService
					.sortByGroups(webinars.get(i));
			sortedModuleByGroups.put(i, allModulesIdsWithGroupListWithDuration);
		}
		// map for selfstudy modules by groups in each module duration
		Map<Integer, Map<Integer, List<Integer>>> selfStudyModulesByGroups = new HashMap<>();
		for (int i = maxModuleDuration; i >= 1; i--) {
			Map<Integer, List<Integer>> selfStudyAllModulesIdsWithGroupListWithDuration = schedulerService
					.createSchedule(grid, gridForScheduleWeekStatus,
							sortedModuleByGroups.get(i), i,
							weekNumberForEducation, false);
			selfStudyModulesByGroups.put(i,
					selfStudyAllModulesIdsWithGroupListWithDuration);
		}
		// map for not included modules by groups in each module duration
		Map<Integer, Map<Integer, List<Integer>>> notIncludedModulesInSchedule = new HashMap<>();
		for (int i = maxModuleDuration; i >= 1; i--) {
			Map<Integer, List<Integer>> notIncludedByDuration = schedulerService
					.fillSelfStudyModules(grid, gridForScheduleWeekStatus,
							selfStudyModulesByGroups.get(i), i,
							weekNumberForEducation);
			notIncludedModulesInSchedule.put(i, notIncludedByDuration);
		}
		// groups MAP
		Map<Integer, Group> groupsMap = new HashMap<>();
		for (Group group : groups) {
			groupsMap.put(group.getGroupId(), group);
		}
		// modules MAP
		Map<Integer, Module> modulesMap = new HashMap<>();
		for (Module module : modules) {
			modulesMap.put(module.getModuleId(), module);
		}

		scheduleVO.setWeekNumberEducation(weekNumberForEducation);
		scheduleVO.setGridForScheduleWeekStatus(gridForScheduleWeekStatus);
		scheduleVO.setGroupsMap(groupsMap);
		scheduleVO.setModulesMap(modulesMap);
		scheduleVO.setGrid(grid);
		scheduleVO
				.setNotIncludedModulesInSchedule(notIncludedModulesInSchedule);
		return scheduleVO;
	}

	private Map<Integer, List<Integer>> putAllModulesForWebinars(
			Map<Integer, List<Integer>> in, Map<Integer, List<Integer>> from) {
		if (in.isEmpty()) {
			in.putAll(from);
		} else {
			for (Map.Entry<Integer, List<Integer>> entry : from.entrySet()) {
				Integer moduleId = entry.getKey();

				if (in.containsKey(moduleId)) {

					List<Integer> groupsNotIncludedModule = in.get(moduleId);
					List<Integer> notDublicatedGroups = new ArrayList<>();
					if (!groupsNotIncludedModule.isEmpty()) {
						groupsNotIncludedModule.addAll(entry.getValue());
						Set<Integer> notDublicated = new HashSet<Integer>(
								groupsNotIncludedModule);
						notDublicatedGroups.addAll(notDublicated);
						in.put(moduleId, notDublicatedGroups);
					}
				} else {
					in.put(moduleId, entry.getValue());
				}
			}
		}
		return in;
	}
}
