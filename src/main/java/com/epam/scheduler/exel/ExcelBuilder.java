package com.epam.scheduler.exel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.epam.scheduler.exception.DataException;
import com.epam.scheduler.model.Group;
import com.epam.scheduler.model.Module;
import com.epam.scheduler.model.ScheduleVO;
import com.epam.scheduler.model.WeekScheduleStatus;

public class ExcelBuilder extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		// get data model which is passed by the Spring container
		ScheduleVO scheduleVO = (ScheduleVO) model.get("scheduleVO");
		Map<Integer, List<Integer>> grid = scheduleVO.getGrid();
		Map<Integer, List<WeekScheduleStatus>> gridForScheduleWeekStatus = scheduleVO
				.getGridForScheduleWeekStatus();
		Map<Integer, Module> modulesMap = scheduleVO.getModulesMap();
		Map<Integer, Group> groupsMap = scheduleVO.getGroupsMap();
		int weeksForEducation = scheduleVO.getWeekNumberEducation();
		int cellPositionForWeeksInExel = weeksForEducation + 7;
		// create a new Excel sheet
		HSSFSheet sheet = workbook.createSheet("schedule");
		// create style for header cells
		CellStyle headerStyle = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		headerStyle.setFillForegroundColor(HSSFColor.GREEN.index);
		headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		headerStyle.setFont(font);

		CellStyle styleRed = workbook.createCellStyle();
		styleRed.setFillBackgroundColor(IndexedColors.RED.getIndex());
		styleRed.setFillForegroundColor(IndexedColors.RED.getIndex());
		styleRed.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleRed.setBorderBottom(HSSFCellStyle.BORDER_DASHED);
		styleRed.setBorderTop(HSSFCellStyle.BORDER_DASHED);
		styleRed.setBorderRight(HSSFCellStyle.BORDER_DASHED);
		styleRed.setBorderLeft(HSSFCellStyle.BORDER_DASHED);

		CellStyle lightGreen = workbook.createCellStyle();
		lightGreen.setFillBackgroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		lightGreen.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		lightGreen.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		lightGreen.setBorderBottom(HSSFCellStyle.BORDER_DASHED);
		lightGreen.setBorderTop(HSSFCellStyle.BORDER_DASHED);
		lightGreen.setBorderRight(HSSFCellStyle.BORDER_DASHED);
		lightGreen.setBorderLeft(HSSFCellStyle.BORDER_DASHED);

		CellStyle styleLightBlue = workbook.createCellStyle();
		styleLightBlue.setFillBackgroundColor(IndexedColors.LIGHT_BLUE
				.getIndex());
		styleLightBlue.setFillForegroundColor(IndexedColors.LIGHT_BLUE
				.getIndex());
		styleLightBlue.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleLightBlue.setBorderBottom(HSSFCellStyle.BORDER_DASHED);
		styleLightBlue.setBorderTop(HSSFCellStyle.BORDER_DASHED);
		styleLightBlue.setBorderRight(HSSFCellStyle.BORDER_DASHED);
		styleLightBlue.setBorderLeft(HSSFCellStyle.BORDER_DASHED);

		CellStyle styleYellow = workbook.createCellStyle();
		styleYellow.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
		styleYellow.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleYellow.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleYellow.setBorderBottom(HSSFCellStyle.BORDER_DASHED);
		styleYellow.setBorderTop(HSSFCellStyle.BORDER_DASHED);
		styleYellow.setBorderRight(HSSFCellStyle.BORDER_DASHED);
		styleYellow.setBorderLeft(HSSFCellStyle.BORDER_DASHED);

		CellStyle styleGrey = workbook.createCellStyle();
		styleGrey.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT
				.getIndex());
		styleGrey.setFillForegroundColor(IndexedColors.GREY_25_PERCENT
				.getIndex());
		styleGrey.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleGrey.setBorderBottom(HSSFCellStyle.BORDER_DASHED);
		styleGrey.setBorderTop(HSSFCellStyle.BORDER_DASHED);
		styleGrey.setBorderRight(HSSFCellStyle.BORDER_DASHED);
		styleGrey.setBorderLeft(HSSFCellStyle.BORDER_DASHED);

		CellStyle lightOrange = workbook.createCellStyle();
		lightOrange.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE
				.getIndex());
		lightOrange.setFillForegroundColor(IndexedColors.LIGHT_ORANGE
				.getIndex());
		lightOrange.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		lightOrange.setBorderBottom(HSSFCellStyle.BORDER_DASHED);
		lightOrange.setBorderTop(HSSFCellStyle.BORDER_DASHED);
		lightOrange.setBorderRight(HSSFCellStyle.BORDER_DASHED);
		lightOrange.setBorderLeft(HSSFCellStyle.BORDER_DASHED);

		// create header row
		HSSFRow header = sheet.createRow(0);

		header.createCell(0).setCellValue("Mentor");
		header.getCell(0).setCellStyle(headerStyle);

		header.createCell(1).setCellValue("Mentee");
		header.getCell(1).setCellStyle(headerStyle);

		header.createCell(2).setCellValue("Group Name");
		header.getCell(2).setCellStyle(headerStyle);

		header.createCell(3).setCellValue("Manager");
		header.getCell(3).setCellStyle(headerStyle);

		header.createCell(4).setCellValue("City");
		header.getCell(4).setCellStyle(headerStyle);

		header.createCell(5).setCellValue("Office");
		header.getCell(5).setCellStyle(headerStyle);

		header.createCell(6).setCellValue("Segment");
		header.getCell(6).setCellStyle(headerStyle);
		int cellCount = 7;
		int weekNumber = 1;
		for (int i = cellCount; i < cellPositionForWeeksInExel; i++) {
			header.createCell(i).setCellValue(weekNumber + " week");
			header.getCell(i).setCellStyle(headerStyle);
			weekNumber++;
		}
		Group group;

		// create data rows
		int rowCount = 1;

		for (Map.Entry<Integer, List<Integer>> entry : grid.entrySet()) {
			Integer groupId = entry.getKey();
			List<Integer> weekForEachGroupWithModuleIds = entry.getValue();
			List<WeekScheduleStatus> weekStatus = gridForScheduleWeekStatus
					.get(groupId);
			group = groupsMap.get(groupId);

			HSSFRow row = sheet.createRow(rowCount++);
			String userName = group.getUser().getEmail();
			row.createCell(0).setCellValue(userName);
			row.createCell(1).setCellValue(group.getMentee());
			row.createCell(2).setCellValue(group.getName());
			row.createCell(3).setCellValue(group.getManager());
			row.createCell(4).setCellValue(
					group.getAddress().getCity().getName());
			row.createCell(5).setCellValue(group.getAddress().getStreet());
			row.createCell(6).setCellValue(
					group.getAddress().getSegment().getSegmentNumber());

			for (int i = 0; i < weeksForEducation; i++) {
				int moduleId = weekForEachGroupWithModuleIds.get(i);
				WeekScheduleStatus status = weekStatus.get(i);
				switch (status) {
				case FREE:
					row.createCell(cellCount).setCellValue("");
					break;
				case VACATION:
					row.createCell(cellCount).setCellValue("Vacation");
					row.getCell(cellCount).setCellStyle(styleRed);
					break;
				case LECTURE:
					row.createCell(cellCount).setCellValue(
							modulesMap.get(moduleId).getName());
					row.getCell(cellCount).setCellStyle(lightGreen);
					break;
				case WEBINAR:
					row.createCell(cellCount).setCellValue(
							modulesMap.get(moduleId).getName());
					row.getCell(cellCount).setCellStyle(styleYellow);
					break;
				case SELFSTUDY:
					row.createCell(cellCount).setCellValue(
							modulesMap.get(moduleId).getName());
					row.getCell(cellCount).setCellStyle(styleGrey);
					break;
				default:
					break;
				}
				cellCount++;
			}
			cellCount = 7;
		}
		rowCount++;
		HSSFRow row = sheet.createRow(rowCount);

		for (int i = scheduleVO.getNotIncludedModulesInSchedule().size(); i > 1; i--) {
			if (scheduleVO.getNotIncludedModulesInSchedule().get(i) != null) {
				row.createCell(0).setCellValue(
						"Groups Which Not Include Modules By Duration " + i
								+ ":");

				rowCount = writeNotIncludedModules(scheduleVO
						.getNotIncludedModulesInSchedule().get(i), rowCount,
						modulesMap, groupsMap, sheet, lightOrange,
						styleLightBlue, row);
				row = sheet.createRow(rowCount);
			}
		}
		// for
		row = workbook.getSheetAt(0).getRow(0);
		for (int colNum = 0; colNum < row.getLastCellNum(); colNum++) {
			workbook.getSheetAt(0).autoSizeColumn(colNum);
		}
	}

	private int writeNotIncludedModules(
			Map<Integer, List<Integer>> modulesNotIncludedScheduleByDuration,
			int rowCount, Map<Integer, Module> modulesMap,
			Map<Integer, Group> groupsMap, HSSFSheet sheet,
			CellStyle cellStyle, CellStyle headerStyle, HSSFRow row)
			throws DataException {
		Group group;
		HSSFRow header = sheet.getRow(rowCount);
		header.createCell(1).setCellValue("Module Name");
		header.getCell(1).setCellStyle(headerStyle);
		header.createCell(2).setCellValue("Group Name");
		header.getCell(2).setCellStyle(headerStyle);
		rowCount++;
		row = sheet.createRow(rowCount);
		for (Map.Entry<Integer, List<Integer>> entry : modulesNotIncludedScheduleByDuration
				.entrySet()) {
			int cellCount = 1;
			Integer moduleId = entry.getKey();
			List<Integer> groupsWichNotIncludedInSchedule = entry.getValue();
			if (!groupsWichNotIncludedInSchedule.isEmpty()) {
				row = sheet.getRow(rowCount);
				row = sheet.createRow(rowCount);
				Module module = modulesMap.get(moduleId);
				row.createCell(cellCount);
				row.getCell(cellCount).setCellValue(module.getName());
				cellCount++;
				rowCount++;
				for (Integer groupId : groupsWichNotIncludedInSchedule) {
					row = sheet.createRow(rowCount);
					row = sheet.getRow(rowCount);
					group = groupsMap.get(groupId);
					row.createCell(cellCount).setCellValue(group.getName());
					row.getCell(cellCount).setCellStyle(cellStyle);
					rowCount++;
				}
			}
		}
		return rowCount;
	}
}