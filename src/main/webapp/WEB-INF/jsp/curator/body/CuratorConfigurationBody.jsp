<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="/scheduler/curatorMainPage?page=schedule"
				id="tab_schedules"><fmt:message key="curator.tab.schedules" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=mentor"
				id="tab_mentors"><fmt:message key="curator.tab.mentors" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=group"
				id="tab_groups"><fmt:message key="curator.tab.groups" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=trainer"
				id="tab_trainers"><fmt:message key="curator.tab.trainers" /></a></li>
			<li><a href="#" class="active" id="tab_trainers"><fmt:message
						key="curator.tab.config" /></a></li>
		</ul>
		<div class="content">
			<div>
				<h3>
					<fmt:message key="curator.config.title" />
				</h3>
			</div>
			<div class="content_body">
				<fieldset>
					<legend>
						<fmt:message key="curator.cleating.data" />
					</legend>
					<div class="delete-mentor-button">
						<input id="deleteMentor" type="button"
							value="<fmt:message key="delete.mentors" />">
					</div>
					<div class="delete-group-button-configPage">
						<input id="deleteGroup" type="button"
							value="<fmt:message key="delete.group.all" />" />
					</div>
					<div id="clearing-data-message"></div>
				</fieldset>
				<fieldset>
					<legend>
						<fmt:message key="curator.quarter.duration" />
					</legend>
					<div class="quarter-duration">
						<div class="quarter-duration_select">
							<input id="durationQuarterHidden" type="hidden"
								value="${durationQuarter}" />
							<c:forEach var="week" begin="0" end="14" varStatus="status">
								<c:if test="${status.count eq 8 }">
									<br>
								</c:if>
								<c:choose>
									<c:when test="${(status.count+5) == durationQuarter}">
										<span><input type="radio" id="duration"
											onchange="onChangeDuration(this)" name="duration"
											value="${status.count}" checked="checked">${status.count + 5}</span>
									</c:when>
									<c:otherwise>
										<span><input type="radio" id="duration"
											onchange="onChangeDuration(this)" name=duration
											value="${status.count}">${status.count + 5 }</span>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
						<div class="quarter-duration_current"
							id="quarter-duration_current">
							<fmt:message key="curator.config.current.quarter" />
							${durationQuarter}
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>
						<fmt:message key="form.lacation" />
					</legend>
					<div class="modul-location">
						<div class="location">
							<label><fmt:message key="form.city" /> <br> <select
								id="city-select" onchange="onChange(this)" name="city"
								data-default-value>
									<option value="0">-- Select city --</option>
									<c:forEach var="city" items="${cities }">
										<option value="${city.cityId}">${city.name}</option>
									</c:forEach>
							</select> </label>
						</div>
						<div class="segment">
							<label><fmt:message key="form.segment" /> <br> <select
								id="segment-select" name="segment" data-default-value>
									<option value="0">-- Select segment --</option>
									<c:forEach var="segment" items="${segments }">
										<option value="${segment.segmentId}">${segment.segmentNumber}</option>
									</c:forEach>
							</select> </label>
						</div>
						<div class="location-office">
							<fmt:message key="mentor.table.office" />
							<div id="location-office">
								<label> <select id="street-select" name="street"
									data-default-value>
										<option value="0">-- Select office --</option>
								</select>
								</label>
							</div>
						</div>
						<div class="delete-office-button">
							<input id="deleteOffice" type="submit" name="groupId"
								value="<fmt:message key="delete.office" />">
						</div>
						<div class="add-office-button">
							<input id="addOffice" type="button"
								value="<fmt:message key="add.office" />" />
						</div>
						<div class="add-office-submit">
							<input id="addOffice" type="button" name="groupId"
								formaction="addNewOffice"
								value="<fmt:message key="add.office" />" />
						</div>
						<div class="add-office-cancel">
							<input id="addOffice-cansel" type="button"
								value="<fmt:message key="cancel" />" />
						</div>
						<div id="info-message"></div>
					</div>
				</fieldset>
				<fieldset>
					<legend>
						<fmt:message key="curator.add.new.module" />
					</legend>
					<div class="add-modul">
						<div class="add-fild">
							<fmt:message key="curator.module.name" />
							<br> <input type="text" name="add-modul-name" />
						</div>
						<div class="add-duration">
							<fmt:message key="curator.duration.module" />
							<br> <input id="first-duration" type="radio"
								name="duration-module" value="1" checked="checked">1 <input
								type="radio" name="duration-module" value="2">2 <input
								type="radio" name="duration-module" value="3">3 <input
								type="radio" name="duration-module" value="4">4 <input
								type="radio" name="duration-module" value="5">5
						</div>

						<div class="add-modul-button">
							<input id="addModule" type="submit" name="moduleId"
								value="<fmt:message key="add.module" />">
						</div>
					</div>
				</fieldset>
				<div class="flag-checkbox">
				<div class="periodOn">ON</div>
				<div class="h9">Registration period:</div>
				
					<c:choose>
						<c:when test="${period==true }">
							<input type="checkbox" id="flag" class="checkbox" name="flager" 
								checked="checked">
						</c:when>
						<c:otherwise>
							<input type="checkbox" id="flag" class="checkbox" name="flager" >
						</c:otherwise>
					</c:choose>
					
					<label for="flag"></label>
					<div class="periodOff">OFF</div>
					<div id="message-flag-checkbox"></div>
					</div>
					<div id="message-flag-checkbox"></div>
					<div class="edit-module">
						<table class="config-modul bordered">
							<thead>
								<tr>
									<th class="modul-name"><fmt:message
											key="mentor.table.modul" /></th>
									<th class="duration"><fmt:message
											key="mentor.table.duration" /></th>
									<th class="delete"><fmt:message key="curator.table.delete" /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="module" items="${modules }" varStatus="status">
									<tr>
										<td><input class="modul-name" type="text"
											value="${module.name}" name="${module.moduleId}"
											onkeydown="onEnterPressModule('${module.moduleId}')"
											onfocus="this.select()"
											onblur="this.value=!this.value?'${module.name}':this.value;" /></td>
										<td><input class="modul-duration" type="text"
											value="${module.duration}" name="${module.moduleId}"
											onkeydown="onEnterPressModule('${module.moduleId}')"
											onfocus="this.select()"
											onblur="this.value=!this.value?'${module.duration}':this.value;" /></td>
										<td><input type="checkbox" name="deleteGroup"
											value="${module.moduleId}"></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div class="delete-module-button">
							<input id="deleteModule" type="submit" name="moduleId"
								value="<fmt:message key="curator.table.delete" />">
						</div>
						<div class="delete-module-button">
							<input id="deleteModuleFixed" type="submit" name="moduleId"
								value="<fmt:message key="curator.table.delete" />">
						</div>
						<a href="#" id="top-config-page"> <span class="fa fa-arrow-up"></span></a>
					</div>
				</div>
			</div>
		</div>
	</div>