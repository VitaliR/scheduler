<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">

		<ul class="tabs">
			<li><a href="#" id="tab_schedules" class="active"><fmt:message
						key="curator.tab.schedules" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=mentor"
				id="tab_mentors"><fmt:message key="curator.tab.mentors" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=group"
				id="tab_groups"><fmt:message key="curator.tab.groups" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=trainer"
				id="tab_trainers"><fmt:message key="curator.tab.trainers" /></a></li>
			<li><a href="/scheduler/curatorConfigPage?page=config"
				id="tab_config"><fmt:message key="curator.tab.config" /></a></li>
		</ul>
		<div class="content">
			<div>
				<h3>
					<fmt:message key="curator.schedule.title" />
				</h3>
			</div>
			<form action="curatorCreateScheduleForWebinars"   method="GET">
				<input type="submit" class="generate" value="<fmt:message key="generate.schedule.webinar"/>">
			</form>
			<form action="curatorCreateScheduleByLocation"   method="GET">
				<input type="submit" class="generate" value="<fmt:message key="generate.schedule.city"/>">
			</form>
			<form action="curatorCreateScheduleBySegment"   method="GET">
				<input type="submit" class="generate" value="<fmt:message key="generate.schedule.segments"/>">
			</form>
		</div>
	</div>
</div>