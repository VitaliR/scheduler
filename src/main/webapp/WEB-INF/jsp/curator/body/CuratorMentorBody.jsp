<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="/scheduler/curatorMainPage?page=schedule"
				id="tab_schedules"><fmt:message key="curator.tab.schedules" /></a></li>
			<li><a href="#" id="tab_mentors" class="active"><fmt:message
						key="curator.tab.mentors" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=group"
				id="tab_groups"><fmt:message key="curator.tab.groups" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=trainer"
				id="tab_trainers"><fmt:message key="curator.tab.trainers" /></a></li>
			<li><a href="/scheduler/curatorConfigPage?page=config"
				id="tab_config"><fmt:message key="curator.tab.config" /></a></li>
		</ul>
		<div class="content">
			<div class="content_header">
				<div class="add-group-show">
					<input id="show" type=button
						onClick="location.href='curatorAddFormMentor'"
						value='<fmt:message	key="add.new.mentor" />'>
				</div>
				<div class="content_title">
					<h3>
						<fmt:message key="curator.mentor.list" />
					</h3>
				</div>
				<div class="delete-trainer-button">
					<input id="deleteMentorList" type="button"
						value="<fmt:message key="delete.mentor" />">
				</div>
			</div>
			<div class="content_mentor_body">
				<c:if test="${not empty error}">
					<div class="error_message">
						<c:out value="${error}" />
					</div>
				</c:if>
				<c:if test="${not empty succes}">
					<div class="succes_message">
						<c:out value="${succes}" />
					</div>
				</c:if>
				<div class="content_list-new-user">
					<div class="table-groups">
						<table class="list-mentors bordered">
							<thead>
								<tr>
									<th class="check"><fmt:message
											key="curator.table.head.check" /></th>
									<th class="name"><fmt:message
											key="curator.table.head.name" /></th>
									<th class="office"><fmt:message
											key="curator.table.head.location" /></th>
									<th class="edit"><fmt:message
											key="curator.table.head.edit" /></th>
									<th class="delete"><fmt:message key="curator.table.delete" /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="mentor" items="${users}" varStatus="status">
									<tr>
										<td>${status.count}</td>
										<td>${mentor.email}</td>
										<td class="location">${mentor.address.city.name}<br>${mentor.address.street}</td>
										<td class="edit"><a href="#"> <fmt:message
													key="curator.table.head.edit" />
										</a></td>
										<td class="delete"><input type="checkbox"
											name="${mentor.email}" value="${mentor.email}"></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<label> <c:if test="${not empty exEmails}">
								<br>
								<div class="error_message">
									<fmt:message key="curator.exist.user.list" />

								</div>
								<table class="list-mentors bordered">
									<thead>
										<tr>
											<th class="check"><fmt:message
													key="curator.table.head.check" /></th>
											<th class="name"><fmt:message
													key="curator.table.head.name" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="email" items="${exEmails }" varStatus="status">
											<tr>
												<td>${status.count}</td>
												<td>${email}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>
						</label> <label> <c:if test="${not empty notValid}">
								<br>
								<div class="error_message">
									<fmt:message key="curator.not.valid.user.list" />

								</div>
								<table class="list-mentors bordered">
									<thead>
										<tr>
											<th class="check"><fmt:message
													key="curator.table.head.check" /></th>
											<th class="name"><fmt:message
													key="curator.table.head.name" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="email" items="${notValid }" varStatus="status">
											<tr>
												<td>${status.count}</td>
												<td>${email}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>
						</label>
					</div>
					<a href="#" id="top-mentor-content"> <span
						class="fa fa-arrow-up"></span></a>
				</div>
			</div>
		</div>
	</div>
</div>