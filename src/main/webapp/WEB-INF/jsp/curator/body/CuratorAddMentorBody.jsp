<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">

		<ul class="tabs">
			<li><a href="/scheduler/curatorMainPage?page=schedule"
				id="tab_schedules"><fmt:message key="curator.tab.schedules" /></a></li>
			<li><a href="#" id="tab_mentors" class="active"><fmt:message
						key="curator.tab.mentors" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=group"
				id="tab_groups"><fmt:message key="curator.tab.groups" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=trainer"
				id="tab_trainers"><fmt:message key="curator.tab.trainers" /></a></li>
			<li><a href="/scheduler/curatorConfigPage?page=config"
				id="tab_config"><fmt:message key="curator.tab.config" /></a></li>
		</ul>
		<div class="content">
			<div>
				<h3>
					<fmt:message key="curator.add.new.mentor" />
				</h3>
			</div>
			<div class="form_add_new_user">
				<form action="curatorAddNewMentor" method="post" class="login active"
					onsubmit="return checkForm(this);" enctype="multipart/form-data">
					<input type="checkbox" id="check" name="check" value="true" />
					<fmt:message key="curator.upload.manualy" />
					<div class="load-file">
						<label><fmt:message key="curator.upload.file" /> <br>
							<input type="file" name="file"
							accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
							value="<fmt:message key="browse" />" /> </label>
					</div>
					<div class="upload-user">
						<label>
						<c:if test="${not empty error}">
								<div class="error_message">
									<c:out value="${error}" />
								</div>
							</c:if> <c:if test="${not empty succes}">

								<div class="succes_message">
									<c:out value="${succes}" />
								</div>
							</c:if> 
							<fmt:message key="curator.enter.email" /> <input type="email"
							name="email" />
						</label>
					</div>
					<div class="add_mentor_button">
						<div class="bottom">
							<input type="submit"
								value="<fmt:message key="curator.create.user.button" />" />
						</div>
						<div class="bottom">
							<input type="button"
								onClick="location.href='curatorMainPage?page=mentor'"
								value='<fmt:message	key="cancel" />'>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>