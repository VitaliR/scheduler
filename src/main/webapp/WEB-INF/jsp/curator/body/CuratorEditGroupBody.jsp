<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="/scheduler/curatorMainPage?page=schedule"
				id="tab_schedules"><fmt:message key="curator.tab.schedules" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=mentor"
				id="tab_mentors"><fmt:message key="curator.tab.mentors" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=group"
				class="active" id="tab_groups"><fmt:message
						key="curator.tab.groups" /></a></li>
			<li><a href="/scheduler/curatorMainPage?page=trainer"
				id="tab_trainers"><fmt:message key="curator.tab.trainers" /></a></li>
			<li><a href="/scheduler/curatorConfigPage?page=config"
				id="tab_config"><fmt:message key="curator.tab.config" /></a></li>
		</ul>
		<div class="content" onscroll="Onscrollfnction();">
			<div>
				<h3>
					<fmt:message key="curator.edit.group" />
				</h3>
			</div>
			<form name="editGroupByCurator" action="curatorEditGroup"
				method="post" onsubmit="return checkForm(this);"
				class="edit-form-group">
				<div class="edit_new_group">
					<div>
						<label><fmt:message key="mentor.form.group.name" /> <sup>*</sup><input
							type="text" name="groupName" value="${group.name }" /> </label>
					</div>
					<div>
						<label> <fmt:message key="mentor.form.mentee.name" /> <sup>*</sup><input
							type="text" name="menteeName" value="${group.mentee }" />
						</label>
					</div>
					<div>
						<label> <fmt:message key="mentor.form.manager.name" /> <sup>*</sup><input
							type="text" name="managerName" value="${group.manager }" />
						</label>
					</div>
					<div class="selector-mentor">
						<label><fmt:message key="curator.mentor.name" /><sup>*</sup> <br>
							<select id="mentor-select" class="mentor-select"
							name="mentorName" data-default-value>
								<option value="0">-- Select mentor --</option>
								<c:forEach var="mentor" items="${mentors }">
									<c:choose>
										<c:when test="${group.user.email==mentor.email}">
											<option value="${mentor.email}" selected="selected">${mentor.email}</option>
										</c:when>
										<c:otherwise>
											<option value="${mentor.email}">${mentor.email}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>

						</select> </label>
					</div>
					<fmt:message key="form.lacation" />
					<br>
					<div class="location">
						<label><fmt:message key="form.city" /> <sup>*</sup><br> <select
							id="city-select" onchange="onChange(this)" name="city"
							data-default-value>
								<option value="0">-- Select City --</option>
								<c:forEach var="city" items="${cities }">
									<c:choose>
										<c:when test="${group.address.city.cityId==city.cityId}">
											<option value="${city.cityId}" selected="selected">${city.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${city.cityId}">${city.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select> </label>
					</div>
					<div class="location">
						<label><fmt:message key="mentor.table.office" /> <sup>*</sup><br>
							<select id="street-select" name="street" data-default-value>
								<option value="0">-- Select office --</option>
								<c:forEach var="address" items="${addressies }">
									<c:choose>
										<c:when test="${group.address.addressId==address.addressId}">
											<option value="${address.street}" selected="selected">${address.street}</option>
										</c:when>
										<c:otherwise>
											<option value="${address.street}">${address.street}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select> </label>
					</div>
				</div>
				<div class="add-modules">
					<div class="form-moduls">
						<div class="form-moduls_vacation">
							<h6>
								<fmt:message key="mentor.week.vacation" />
							</h6>
							<div>

								<c:forEach var="week" begin="1" end="${weekDuration}"
									varStatus="status">
									<c:set var="checked" value="false" />
									<c:forEach var="vacation" items="${vacations }">
										<c:if test="${vacation.weekNumber==week}">
											<label> <input type="checkbox" name="weekVocation"
												value="${status.count}" checked="checked">
											</label>
											<c:set var="checked" value="true" />
										</c:if>
									</c:forEach>
									<c:if test="${!checked}">
										<label> <input type="checkbox" name="weekVocation"
											value="${status.count}">
										</label>
										<c:set var="checked" value="false" />
									</c:if>
									<fmt:message key="mentor.checkbox.week.vacation" /> ${status.count}<br>
								</c:forEach>
							</div>
						</div>
						<div class="form-moduls_choice-modul">
							<h6>
								<fmt:message key="mentor.choice.modul" />
							</h6>
							<fmt:message key="curator.module.anotation" />
							<select id="my-select" multiple="multiple" name="module"
								class="multiselect">
								<c:forEach var="module" items="${modules }" varStatus="status">
									<c:set var="checked" value="false" />
									<c:forEach var="groupModule" items="${group.modules}">
										<c:if test="${groupModule.moduleId==module.moduleId}">
											<option value="${module.moduleId}"
												data-info='${module.duration}' selected="selected">${module.name}(${module.duration})</option>
											<c:set var="checked" value="true" />
										</c:if>
									</c:forEach>
									<c:if test="${!checked}">
										<option value="${module.moduleId}"
											data-info='${module.duration}'>${module.name}(${module.duration})</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
						<div class="form-moduls_count-modules">
							<c:set var="weekDuration" value="${weekDuration}" />
							<h6>
								<fmt:message key="mentor.count.modules" />
								<br>
								<fmt:message key="mentor.total.duration">
									<fmt:param value="${weekDuration}" />
								</fmt:message>
							</h6>
							<div id="count-moduls"></div>

							<input id="add-group-button" type="submit"
								value="<fmt:message key="update.group" />"
								formaction="curatorEditGroup?groupId=${group.groupId }"
								formmethod="post" /> <input id="cancel-edit" type="submit"
								formaction="curatorAddGroupLink?groupId=${group.groupId }"
								formmethod="get" value="<fmt:message key="cancel" /> " />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



