<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="#" id="tab_schedules" class="active"><fmt:message
						key="curator.tab.groups" /></a></li>
		</ul>
		<div class="content">
			<div class="content_header">
				<div class="add-group-show">
					<input type="submit" id="show"
						value="<fmt:message key="add.group" />" />
				</div>
				<div class="content_title">
					<h3>
						<fmt:message key="mentor.group.create" />
					</h3>
				</div>
				<div class="delete-group-button">
					<input id="deleteGroup" type="submit" name="groupId"
						form="deleteGroupForm" value="<fmt:message key="delete" />">
				</div>
			</div>
			<div class="content_group">
				<form name="createMentorGroup" action="mentorCreateGroup"
					method="post" onsubmit="return checkForm(this);"
					class="add-form-group">
					<div id="add_new_group" class="add_new_group"
						onscroll="scrollPos();">
						<div>
							<label><fmt:message key="mentor.form.group.name" /> <sup>*</sup><input
								type="text" name="groupName" value="${group.name }" /> </label>
						</div>
						<div>
							<label> <fmt:message key="mentor.form.mentee.name" /><sup>*</sup> <input
								type="text" name="menteeName" value="${group.mentee }" />
							</label>
						</div>
						<div>
							<label> <fmt:message key="mentor.form.manager.name" /><sup>*</sup> <input
								type="text" name="managerName" value="${group.manager }" />
							</label>
						</div>
						<div class="location">
							<label><fmt:message key="form.city" /><sup>*</sup> <br> <select
								id="city-select" onchange="onChange(this)" name="city"
								data-default-value>
									<option value="">-- Select city --</option>
									<c:forEach var="city" items="${cities }">
										<option value="${city.cityId}">${city.name}</option>
									</c:forEach>
							</select> </label>
						</div>
						<div class="location">
							<label><fmt:message key="mentor.table.office" /><sup>*</sup> <br>
								<select id="street-select" name="street" data-default-value>
									<option value="0">-- Select office --</option>
							</select> </label>
						</div>
					</div>
					<div class="add-modules">
						<div class="form-moduls">
							<div class="form-moduls_vacation">
								<h6>
									<fmt:message key="mentor.week.vacation" />
								</h6>
								<div>
									<c:forEach var="week" begin="1" end="${weekDuration}"
										varStatus="status">
										<label><input type="checkbox" name="weekVocation"
											value="${status.count}"> <fmt:message
												key="mentor.checkbox.week.vacation" /> ${status.count}<br>
										</label>
									</c:forEach>
								</div>
							</div>
							<div class="form-moduls_choice-modul">
								<h6>
									<fmt:message key="mentor.choice.modul" />
								</h6>
								<fmt:message key="curator.module.anotation" />
								<select id="my-select" multiple="multiple" name="module"
									class="multiselect">
									<c:forEach var="module" items="${modules }" varStatus="status">
										<option value="${module.moduleId}"
											data-info='${module.duration}'>${module.name}(${module.duration})</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-moduls_count-modules">
								<c:set var="weekDuration" value="${weekDuration}" />
								<h6>
									<fmt:message key="mentor.count.modules" />
									<br>
									<fmt:message key="mentor.total.duration">
										<fmt:param value="${weekDuration}" />
									</fmt:message>
								</h6>
								<div id="count-moduls"></div>
								<input id="add-group-button" type="submit"
									value="<fmt:message key="add.group" />" /> <input id="hide"
									type="reset" value="<fmt:message key="cancel" />" /> <input
									id="deleteGroupWithAddGroup" type="submit" name="groupId"
									form="deleteGroupForm" value="<fmt:message key="delete" />">
							</div>
						</div>
					</div>
				</form>
				<div class="form-group-curator">
					<form id="deleteGroupForm" name="deleteGroup"
						action="mentorDeleteGroup" method="POST"
						onsubmit="return deleteListOfGroup(this);">
<!-- 						<div class="table-groups"> -->
							<table class="bordered">
								<thead>
									<tr >
										<th class="check"><fmt:message key="mentor.number" /></th>
										<th class="name"><fmt:message key="curator.table.group" /></th>
										<th class="name"><fmt:message key="curator.table.mentee" /></th>
										<th class="modul"><fmt:message key="mentor.table.modul" /></th>
										<th class="vacation"><fmt:message
												key="mentor.table.vacation" /></th>
										<th class="office"><fmt:message key="curator.table.head.location" /></th>
										<th class="edit"><fmt:message
												key="curator.table.head.edit" /></th>
										<th class="delete"><fmt:message
												key="curator.table.delete" /></th>
									</tr>
								</thead>
								<c:forEach var="group" items="${groups}" varStatus="status">
									<c:set var="moduless" value="${group.modules}" />
									<c:set var="vocations" value="${group.vacations}" />
									<tr>
										<td >${status.count}</td>
										<td >${group.name}</td>
										<td >${group.mentee}</td>
										<td class="modules"><c:forEach var="moduler" items="${moduless}">
									${moduler.name}<br>
											</c:forEach></td>
										<td class="vacations"><c:forEach var="vocation" items="${vocations}">
									${vocation.id} <fmt:message key="mentor.checkbox.week.vacation" />
												<br>
											</c:forEach></td>
										<td class="location">${group.address.city.name}<br>${group.address.street}</td>
										<td class="edit"><a href="mentorEditGroupLink?groupId=${group.groupId}"> <fmt:message key="curator.edit.link" />
										</a></td>
										<td class="delete"><input type="checkbox" id="${group.groupId}" name="deleteGroup"
											value="${group.groupId}">
											<label for="${group.groupId}"></label></td>
									</tr>
								</c:forEach>
							</table>
					</form>
					<a href="#" id="top"> <span class="fa fa-arrow-up"></span></a>
				</div>
			</div>
		</div>
	</div>
</div>



