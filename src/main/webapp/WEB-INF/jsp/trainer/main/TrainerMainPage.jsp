<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title><fmt:message key="trainer.title" /></title>
<link href="<c:url value="/css/curator.css" />" rel="stylesheet">
<link href="<c:url value="/css/multi-select.css"/>" media="screen"
	rel="stylesheet" type="text/css">
<link href="<c:url value="/css/jquery-ui.css"/>" rel="stylesheet">
<link href="<c:url value="/img/switch.png"/>">
<link href="<c:url value="css/font-awesome.min.css"/>" rel="stylesheet">

<link href="<c:url value="/img/favicon.ico"/>" rel="shortcut icon" />

<script src="<c:url value="js/jquery-2.1.3.js"/>"></script>
<script src="<c:url value="js/jquery-ui.js"/>"></script>
<script src="<c:url value="js/jquery.multi-select.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="js/validateForm.js"/>" type="text/javascript"></script>
<script src="<c:url value="js/script.js"/>" type="text/javascript"></script>
</head>
<body>
	<jsp:include page="/WEB-INF/jsp/header/header.jsp"></jsp:include>
	<jsp:include page="${page}"></jsp:include>
</body>
</html>