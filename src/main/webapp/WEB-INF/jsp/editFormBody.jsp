<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="#" class="active" id="tab_modules"><fmt:message
						key="edit.profile" /></a></li>
		</ul>
		<div class="content">

			<form name="addNewTrainer" action="editUserProfile" method="post"
				class="login active" onsubmit="return checkForm(this);">

				<div class="add_new_group">
					<%-- 						<fmt:message key="form.login" />${pageContext.request.userPrincipal.name} --%>
					<!-- 						Login!!!! -->
					<div>
						<label><fmt:message key="form.firstName" /> <sup>*</sup><br><input
							type="text" name="firstName" value="${user.firstName }" /> </label>
					</div>
					<div>
						<label> <fmt:message key="form.lastName" /><sup>*</sup><br> <input
							type="text" name="lastName" value="${user.lastName }" />
						</label>
					</div>
					<div>
						<label><fmt:message key="form.new.password" /><sup>*</sup><br><input
							type="password" name="password" /> </label>
					</div>
					<div>
						<label><fmt:message key="form.confirm.new.password" /><sup>*</sup><br><input
							type="password" name="confirmPassword" /> </label>
					</div>
					<div class="location">
						<label><fmt:message key="form.city" /> <sup>*</sup><br>
							<select id="city-select" onchange="onChange(this)" name="city"
							data-default-value>
								<option value="0">-- Select City --</option>
								<c:forEach var="city" items="${cities }">
									<c:choose>
										<c:when test="${user.address.city.cityId==city.cityId}">
											<option value="${city.cityId}" selected="selected">${city.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${city.cityId}">${city.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select> </label>
					</div>
					<div class="location">
						<label><fmt:message key="mentor.table.office" /> <sup>*</sup><br>
							<select id="street-select" name="street" data-default-value>
								<option value="0">-- Select office --</option>
								<c:forEach var="address" items="${addressies }">
									<c:choose>
										<c:when test="${user.address.addressId==address.addressId}">
											<option value="${address.street}" selected="selected">${address.street}</option>
										</c:when>
										<c:otherwise>
											<option value="${address.street}">${address.street}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select> </label>
					</div>
				</div>
				<input type="hidden" name="email" value="${user.email}" />
				<div class="bottom">
					<input type="submit" value="<fmt:message key="edit.apply" />" />
				</div>
			</form>
		</div>
	</div>
</div>
