<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/css/curator.css" />" rel="stylesheet">
<link href="<c:url value="/css/multi-select.css"/>" media="screen"
	rel="stylesheet" type="text/css">
<link href="<c:url value="/css/jquery-ui.css"/>" rel="stylesheet">
<link href="<c:url value="/img/switch.png"/>">
<link href="<c:url value="css/font-awesome.min.css"/>" rel="stylesheet">

<link href="<c:url value="/img/favicon.ico"/>" rel="shortcut icon" />

<script src="<c:url value="js/jquery-2.1.3.js"/>"></script>
<script src="<c:url value="js/jquery-ui.js"/>"></script>
<script src="<c:url value="js/jquery.multi-select.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="js/validateForm.js"/>" type="text/javascript"></script>
<script src="<c:url value="js/script.js"/>" type="text/javascript"></script>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/header/headerRegistration.jsp"></jsp:include>
	<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="#" class="active" id="tab_modules"><fmt:message
						key="trainer.tab.registerForm" /></a></li>
		</ul>
		<div class="content">
			<div>
				<h3>
					<fmt:message key="form.fill.out" />
				</h3>
			</div>

			<form name="addNewTrainer" action="editUserProfile" method="post"
				class="login active" onsubmit="return checkForm(this);">

				<div class="add_new_group">
					<%-- 						<fmt:message key="form.login" />${pageContext.request.userPrincipal.name} --%>
					<!-- 						Login!!!! -->
					<div>
						<label><fmt:message key="form.firstName" /><sup>*</sup> <input
							type="text" name="firstName"  /> </label>
					</div>
					<div>
						<label> <fmt:message key="form.lastName" /> <sup>*</sup><input
							type="text" name="lastName"  />
						</label>
					</div>
					<div>
						<label><fmt:message key="form.new.password" /><sup>*</sup><input
							type="password" name="password" /> </label>
					</div>
					<div>
						<label><fmt:message key="form.confirm.new.password" /><sup>*</sup><input
							type="password" name="confirmPassword"  /> </label>
					</div>
					<div class="location">
						<label><fmt:message key="form.city" /> <sup>*</sup><br> <select
							id="city-select" onchange="onChange(this)" name="city"
							data-default-value>
								<option value="">-- Select city --</option>
								<c:forEach var="city" items="${cities }">
									<option value="${city.cityId}">${city.name}</option>
								</c:forEach>
						</select> </label>
					</div>
					<div class="location">
						<label><fmt:message key="mentor.table.office" /><sup>*</sup> <br>
							<select id="street-select" name="street" data-default-value>
								<option value="0">-- Select office --</option>
						</select> </label>
					</div>
				</div>
				<input type="hidden" name="email" value="${user.email}" />
				<div class="bottom">
					<input type="submit" value="<fmt:message key="register" />" />
				</div>
			</form>
		</div>
	</div>
</div>

</body>
</html>

