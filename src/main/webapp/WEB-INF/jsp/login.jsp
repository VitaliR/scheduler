<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title><fmt:message key="login.title" /></title>
<link href="<c:url value="/css/form_login.css" />" rel="stylesheet">
<script src="<c:url value="js/jquery-2.1.3.js"/>"></script>
<script src="<c:url value="js/jquery-ui.js"/>"></script>
<script src="<c:url value="js/validateForm.js"/>" type="text/javascript"></script>
</head>
<body>
	<div id="form_wrapper" class="form_wrapper">
		<div>
			<h3>
				<fmt:message key="login.title.form" />
			</h3>
		</div>
		<form action="<c:url value='j_spring_security_check'/>" method="post"
			class="login active" onsubmit="return checkForm(this);">
			<div>
				<label> <fmt:message key="login.email" /> <c:if
						test="${not empty succes}">

						<div class="succes_message">
							<c:out value="${succes}" />
						</div>
					</c:if> <c:if test="${not empty error}">

						<div class="error_message">
							<c:out value="${error}" />
						</div>
					</c:if> <input type="email" name="j_username" />


				</label>
			</div>
			<div>
				<label><fmt:message key="login.password" /><input
					type="password" name="j_password" /> </label>
			</div>
			<div class="address_curator">
				<a href="<fmt:message key="login.email.address.curator" />"><fmt:message
						key="login.contact.curator" /></a>
			</div>

			<div class="bottom">
				<input type="submit" value="<fmt:message key="login.button" />" />
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>
</body>
</html>