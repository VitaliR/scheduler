$(function() {
	$('.content_mentor-group').on('scroll', scrollFixButtonDelete)
});
$(function() {
	$('.content_group').on('scroll', scrollFixButtonDelete)
});

$(function() {
	$('.content_body').on('scroll', scrollDeleteButton)
});
$(function() {
	$('.content_list-new-user').on('scroll', scrollMentorContent)
});
$(function() {
	$('.content_list-new-user-trainer').on('scroll', scrollTrainerContent)
});

$(function() {
	$('#deleteMentorList').click(deleteMentorList)

})
$(function() {
	$('#deleteTrainerList').click(deleteTrainerList)

})
function deleteTrainerList() {
	var listDeleteTrainers = $('.list-trainers input[type=checkbox]:checked')
			.map(function(_, el) {
				return $(el).val();
			}).get();
	if (listDeleteTrainers.length == 0) {
		alert("Not checked!");
		return false;
	}
	if (confirmDialogWindow("Want to delete trainers?")) {
		$.ajax({
			url : 'curatorDeleteTrainerList',
			contentType : "application/json",
			type : 'DELETE',
			data : JSON.stringify(listDeleteTrainers),
			dataType : 'json',
			success : function(data, textStatus, jqXHR) {
				if (data.length > 0) {
					for (var i = 0; i < data.length; i++) {
						$('.list-trainers input[name="' + data[i] + '"]')
								.closest('tr').remove();
					}
					for (var i = 0; i < listDeleteTrainers.length; i++) {
						if ($('.list-trainers input[name="'
								+ listDeleteTrainers[i] + '"]') != null) {
							$(
									'.list-trainers input[name="'
											+ listDeleteTrainers[i] + '"]')
									.prop('checked', false);
							$(
									'.list-trainers input[name="'
											+ listDeleteTrainers[i] + '"]')
									.parent('td').text('Check group!').css(
											'color', 'red');
						}
					}
					var listNumber = $('.list-trainers tr>td:first-child').map(
							function(_, el) {
								return $(el).val();
							}).get();
					for (var i = 1; i <= listNumber.length; i++) {
						$(
								'.list-trainers tbody >tr:nth-child(' + i
										+ ')>td:first-child').text(i);
					}
				}else{
					alert("All trainers used groups! Delete  groups.");
				}
			},
			error : function(xhr, status, errorThrown) {
				alert("Opsss...Internal server error!");
			},
		});
	}
}
function deleteMentorList() {
	var listDeleteMentors = $('.list-mentors input[type=checkbox]:checked')
			.map(function(_, el) {
				return $(el).val();
			}).get();
	if (listDeleteMentors.length == 0) {
		alert("Not checked!");
		return false;
	}
	if (confirmDialogWindow("Want to delete mentors?")) {
		$.ajax({
			url : 'curatorDeleteMentorList',
			contentType : "application/json",
			type : 'DELETE',
			data : JSON.stringify(listDeleteMentors),
			dataType : 'json',
			success : function(data, textStatus, jqXHR) {
				if (data.length > 0) {
					for (var i = 0; i < data.length; i++) {
						$('.list-mentors input[name="' + data[i] + '"]')
								.closest('tr').remove();
					}
					for (var i = 0; i < listDeleteMentors.length; i++) {
						if ($('.list-mentors input[name="'
								+ listDeleteMentors[i] + '"]') != null) {
							$(
									'.list-mentors input[name="'
											+ listDeleteMentors[i] + '"]')
									.prop('checked', false);
							$(
									'.list-mentors input[name="'
											+ listDeleteMentors[i] + '"]')
									.parent('td').text('Check group!').css(
											'color', 'red');
						}
					}
					var listNumber = $('.list-mentors tr>td:first-child').map(
							function(_, el) {
								return $(el).val();
							}).get();
					for (var i = 1; i <= listNumber.length; i++) {
						$(
								'.list-mentors tbody >tr:nth-child(' + i
										+ ')>td:first-child').text(i);
					}
				} else {
					alert("All mentors created groups! Delete  groups.");
				}
			},
			error : function(xhr, status, errorThrown) {
				alert("Opsss...Internal server error!");
			},
		});
	}
}
function scrollMentorContentResolve() {
	var duration = 300, sizeWindow = '4.6%';
	if ($(window).width() < 1300) {
		sizeWindow = '5.96%';
	}
	console.log($('.list-mentors').offset().top);
	if ($('.list-mentors').offset().top < 184) {

		$('#top-mentor-content').fadeIn(duration);
	} else {
		$('#top-mentor-content').fadeOut(duration);
	}
};
function scrollTrainerContentResolve() {
	var duration = 300, sizeWindow = '4.6%';
	if ($(window).width() < 1300) {
		sizeWindow = '5.96%';
	}
	console.log($('.list-mentors').offset().top);
	if ($('.list-trainer').offset().top < 184) {

		$('#top-trainer-content').fadeIn(duration);
	} else {
		$('#top-trainer-content').fadeOut(duration);
	}
};
function configCuratorPageScrollDeleteButton() {
	var duration = 300, sizeWindow = '4.6%';
	var p = document.getElementById('deleteGroup');
	var position = $('#deleteGroup').position();
	if ($(window).width() < 1300) {
		sizeWindow = '5.96%';
	}
	if ($('#deleteModule').offset().top < 150) {
		$('#deleteModuleFixed').fadeIn(duration);
		$('#top-config-page').fadeIn(duration);
	} else {
		$('#deleteModuleFixed').fadeOut(duration);

		$('#top-config-page').fadeOut(duration);
	}
};
function a() {
	var duration = 300, sizeWindow = '4.6%';
	if ($(window).width() < 1300) {
		sizeWindow = '5.96%';
	}
	if ('none' == $('#show').css('display')) {

		if ($('#deleteGroupForm').offset().top < 170) {
			$('#deleteGroup').show('slow');
			$('#deleteGroupWithAddGroup').hide('slow');
			$('#top').fadeIn(duration);
		} else {
			$('#deleteGroup').hide('slow');
			$('#deleteGroupWithAddGroup').show('slow');

			$('#top').fadeOut(duration);
		}
	} else {
		if ($('#deleteGroupForm').offset().top < 170) {
			//			
			$('#top').fadeIn(duration);
		} else {
			//			
			$('#top').fadeOut(duration);
		}
	}
};
var scrollFixButtonDelete = a;
var scrollDeleteButton = configCuratorPageScrollDeleteButton;
var scrollMentorContent = scrollMentorContentResolve;
var scrollTrainerContent = scrollTrainerContentResolve;
jQuery(document).ready(function() {
	var offset = 190;
	var duration = 300;
	jQuery('#top').click(function(event) {
		event.preventDefault();
		jQuery('.content_group').animate({
			scrollTop : 0
		}, duration);
		return false;
	})
});
jQuery(document).ready(function() {
	var offset = 190;
	var duration = 300;
	jQuery('#top-config-page').click(function(event) {
		event.preventDefault();
		jQuery('.content_body').animate({
			scrollTop : 0
		}, duration);
		return false;
	})
});
jQuery(document).ready(function() {
	var offset = 190;
	var duration = 300;
	jQuery('#top-mentor-content').click(function(event) {
		event.preventDefault();
		jQuery('.content_list-new-user').animate({
			scrollTop : 0
		}, duration);
		return false;
	})
});
function onChange(element) {
	var cityId = $("#city-select").val();
	var getStreetUrlById = 'getAddressiesByCityId/' + cityId;
	if (0 == cityId) {
		return false;
	}
	$.ajax({
		url : getStreetUrlById,
		contentType : "application/json",
		type : 'GET',
		dataType : 'json',
		success : function(response) {
			createOfficeSelector(response);
		},
		error : function(xhr, status, errorThrown) {
			console.log('Status: ' + status);
			console.log('Error: ' + errorThrown);
			cleanSelect();
		},
	});
}
function onChangeDuration(element) {
	var durationId = $('input[name="duration"]:checked').val();
	var durationQuarterHidden = $('#durationQuarterHidden').val();
	if (confirmDialogWindow("Want to change duration?")) {
		var changeDurationQuarter = 'curatorChangeDurationQuarter/'
				+ (parseInt(durationId) + 5);

		$
				.ajax({
					url : changeDurationQuarter,
					contentType : "application/json",
					type : 'GET',
					dataType : 'json',
					success : function(response) {
						if (response > 0) {
							currentDuratin(response);
							$('#durationQuarterHidden').val(
									parseInt(durationId) + 5);
						} else {
							alert("You can't change duration!\n Delete list of groups.");
							$(
									'.quarter-duration_select input[value="'
											+ (durationQuarterHidden - 5)
											+ '"]').prop('checked', true);
						}

					},
					error : function(xhr, status, errorThrown) {
						alert("Opsss...Internal server error!");
					},
				});
	} else {
		$(
				'.quarter-duration_select input[value="'
						+ (durationQuarterHidden - 5) + '"]').prop('checked',
				true);
	}
}
function cleanSelect() {
	var selectList = document.getElementById("street-select");
	var output = '<option value="0">-- Select office --</option>';
	selectList.innerHTML = output;
}
function currentDuratin(responseText) {
	var selectList = document.getElementById("quarter-duration_current");
	var output = 'Current quarter duration - ' + responseText + ' week.';
	selectList.innerHTML = output;
}
function createOfficeSelector(responseText) {

	var selectList = document.getElementById("street-select");
	if (selectList == null) {
		return false;
	}
	var output = '<option value="0">-- Select office --</option>';

	for (var i = 0; i < responseText.length; i++) {

		output = output + '<option value="' + responseText[i] + '">'
				+ responseText[i] + '</option>';
	}
	selectList.innerHTML = output;
}
function createSchedule() {
	download('test.txt', 'Hello world!');
}
function download(filename, text) {
	var pom = document.createElement('a');
	pom.setAttribute('href', 'data:text/plain;charset=utf-8,'
			+ encodeURIComponent(text));
	pom.setAttribute('download', filename);

	if (document.createEvent) {
		var event = document.createEvent('MouseEvents');
		event.initEvent('click', true, true);
		pom.dispatchEvent(event);
	} else {
		pom.click();
	}
}
function confirmDialogWindow(text) {
	var answer = confirm(text);
	if (answer) {
		return true;
	} else {
		return false;
	}
}
