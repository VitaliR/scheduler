﻿function deleteListOfGroup(form) {
	var chks = $('input[name=deleteGroup]:checked').map(function(_, el) {
		return $(el).val();
	}).get();
	if (chks.length == 0) {
		alert("Not checked!");
		return false;
	} else {
		return confirmDelete();
	}
}
function confirmDelete() {
	var answer = confirm("Want to delete?");
	if (answer) {
		return true;
	} else {
		return false;
	}
}

$(function() {
	$('#my-select').multiSelect();
});
$(document).ready(function() {
	//	
	$("#hide").click(function() {
		$(".add-form-group").hide('slow');
		$("#show").show('slow');
		$("#hide").hide('slow');
		$('#deleteGroup').show('slow');
		resetModulesAfterCancel();
	});
	$("#show").click(function() {
		$("#show").hide('slow');
		$("#hide").show('slow');
		$('#deleteGroup').hide('slow');
		$(".add-form-group").show('slow');
		$(".add-form-group").css('display','flex');

		a();
	});
});

var resetModulesAfterCancel = function() {
	var selectList = document.getElementById("street-select");
	var output = '<option value="">-- Select office --</option>';
	selectList.innerHTML = output;

	$('.ms-selection ul > li').css('display', 'none')
	$('.ms-selection ul > li').attr('class', 'ms-elem-selection');
	$('.ms-selectable ul > li').css('display', 'list-item')
	$('.ms-selectable ul > li').attr('class', 'ms-elem-selectable');
	$("#my-select > option:selected").prop("selected", false);
	clickModulesValidate();
}
$(function() {
	$("#hide").hide();
	$(".add-form-group").hide();
});

var clickModulesValidate = function() {
	var element = 0;
	var obj = $(this).children('option:selected').data('info');
	var types = $('#my-select > option:selected').map(function() {
		return this.getAttribute('data-info');
	}).get().sort();

	for (var sum = 0; sum < types.length; sum++) {
		element = element + parseInt(types[sum]);
	}
	var weekVacationDuration = $('.form-moduls_vacation input:checkbox:last')
			.val();
	var weekVacation = $('input[type=checkbox]:checked').map(function(_, el) {
		return $(el).val();
	}).get().sort(function(a, b) {
		return a - b
	});
	var sortWeekVacation = new Array();

	var count = 0;
	if (parseInt(weekVacation[0]) != 1) {
		sortWeekVacation[count] = weekVacation[0] - 1;
		count++;
	}
	for (var i = 0; i < weekVacation.length; i++) {
		var workWeek = weekVacation[i + 1] - weekVacation[i] - 1;
		if (workWeek > 0) {
			sortWeekVacation[count] = workWeek;
			count++;
		}
	}
	if (weekVacation[weekVacation.length - 1] != weekVacationDuration) {
		sortWeekVacation[count] = weekVacationDuration
				- weekVacation[weekVacation.length - 1]
	}
	var divText = 0;
	var div = document.getElementById('count-moduls');
	if (element > 0 || weekVacation.length >= 0) {
		divText = (element + weekVacation.length) + " weeks:<br>" + element
				+ " week(s) for studying<br>" + weekVacation.length
				+ " week(s) for vacation<br>";
		$('#count-moduls').css("color", "#000");
		console.log((element + weekVacation.length) + " "
				+ weekVacationDuration);
		if ((element + weekVacation.length) > weekVacationDuration) {
			$('#count-moduls').css("color", "red");
			divText = (element + weekVacation.length) + " weeks!";
			$("#add-group-button").hide('slow');
		} else if (sortWeekVacation[0] >= 0
				&& (!checkModuleWeekSize(sortWeekVacation.sort(function(a, b) {
					return a - b
				}), types))) {
			divText = "Conflict duration of modules with chosen vacation!";
			$('#count-moduls').css({
				"color" : "red",
				'marging' : '10px'
			});
			$("#add-group-button").hide('slow');
		} else {
			$("#add-group-button").show('slow');
		}
	} else {
		divText = null;
	}
	div.innerHTML = divText;
}
var checkModuleWeekSize = function(sortWeekVacation, types) {
	var flag = false;
	for (var i = types.length - 1; i >= 0; i--) {
		flag = false;
		if (types[i] > sortWeekVacation[sortWeekVacation.length - 1]) {
			return false;
		} else {
			for (var j = 0; j < sortWeekVacation.length; j++) {
				if (sortWeekVacation[j] == types[i]) {
					sortWeekVacation[j] = 0;
					flag = true;
					break;
				}
			}
			if (!flag) {
				sortWeekVacation[sortWeekVacation.length - 1] = sortWeekVacation[sortWeekVacation.length - 1]
						- types[i];
			}
			sortWeekVacation.sort(function(a, b) {
				return a - b
			});
		}
	}
	return true;
}
$(function() {
	$('.form-moduls_vacation input[type=checkbox]').on('change',
			clickModulesValidate);
});
$(function() {
	$('#my-select').on('change', clickModulesValidate)
});
$(function() {
	$('#check[type=checkbox]').on('change', function() {
		$('.upload-user').find('input[name=email]').val('');
	})
});
function detectIE() {
	var ua = window.navigator.userAgent;

	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// IE 12 => return version number
		return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}
	return false;
}
function confirmUpadateIE(versionIE) {
	var answer = confirm("Your browser IE " + versionIE + " not support!"
			+ "Update it to IE 11 or\nuse Google Chrome browser.");
	if (answer) {
		return true;
	} else {
		return false;
	}
}
function checkForm(form) {

	var errorList = [];
	var element, elementNodeName, elementValue, confirmPassword;
	var versionIE = detectIE();
	if (versionIE < 11 && versionIE != false) {
		if (confirmUpadateIE(versionIE)) {
			var url = "http://windows.microsoft.com/en-US/internet-explorer/download-ie";
			var link = document.createElement('a');
			link.href = url;
			document.body.appendChild(link);
			link.click();
			return false;
		} else {
			return false;
		}
	}
	if (($("#street-select").val() !== undefined)
			&& (($("#street-select").val()[0] === undefined) || $(
					"#street-select").val()[0] == 0)) {
		errorList.push("Select city and street\n");
	}
	if (($("#mentor-select").val() !== undefined)
			&& (($("#mentor-select").val()[0] === undefined) || $(
					"#mentor-select").val()[0] == 0)) {
		errorList.push("Select mentor\n");
	}
	var flagEmail = true;
	for (var i = 0; i < form.elements.length; i++) {
		element = form.elements[i];
		elementNodeName = element.nodeName.toLowerCase();
		elementValue = element.value;

		var errorEmail;
		if (elementNodeName == "input") {
			if (element.name == "email") {
				if (elementValue == "") {
					if (flagEmail) {
						errorEmail = "Enter your e-mail\n"
					}
				} else if (!(elementValue
						.match(new RegExp(
								"^\\s*[a-zA-Z0-9]{3,30}\\_[a-zA-Z0-9]{3,34}@epam.com\\s*$")))) {
					if (flagEmail) {
						errorEmail = "Incorrect E-mail\n";
					}
				}
			}
			if (!$('#check').prop('checked')) {
				if (element.name == "file") {
					if (elementValue != "") {
						var val = elementValue;
						switch (val.substring(val.lastIndexOf('.') + 1)
								.toLowerCase()) {
						case 'xlsx':
							flagEmail = false;
							break;
						default:
							errorEmail = "Chose an exсel-file!\n";
							flagEmail = false;
							break;
						}
					}
				}
			}
			if (element.name == "groupName") {
				if (elementValue == "") {
					errorList.push("Enter group name\n");
				} else if (!(elementValue.match(new RegExp(
						"^[\\s*\\w\(\)-]{3,50}$")))) {
					errorList.push("Incorrect group name\n");
				}
			}
			if (element.name == "menteeName") {
				if (elementValue == "") {
					errorList.push("Enter mentee name\n");
				} else if (!(elementValue
						.match(new RegExp(
								"^\\s*[a-zA-Z0-9]{3,30}\\_[a-zA-Z0-9]{3,34}@epam.com\\s*$")))) {
					errorList.push("Incorrect mentee name \n");
				}
			}
			if (element.name == "managerName") {
				if (elementValue == "") {
					errorList.push("Enter manager e-mail\n");
				} else if (!(elementValue
						.match(new RegExp(
								"^\\s*[a-zA-Z0-9]{3,30}\\_[a-zA-Z0-9]{3,34}@epam.com\\s*$")))) {
					errorList.push("Incorrect manager e-mail\n");
				}
			}
			if (element.name == "firstName") {
				if (elementValue == "") {
					errorList.push("Enter your first name\n");
				} else if (elementValue.length > 20) {
					errorList
							.push("Length of first name should be less 20 characters"
									+ "\n");
				}
			}

			if (element.name == "lastName") {
				if (elementValue == "") {
					errorList.push("Enter your last name\n");
				} else if (elementValue.length > 20) {
					errorList
							.push("Length of last name should be less 20 characters"
									+ "\n");
				}
			}
			if (element.name == "password") {
				if (elementValue == "") {
					errorList.push("Enter your password\n");
				} else if (elementValue.length > 60) {
					errorList
							.push("Length of password should be less 60 characters"
									+ "\n");
				} else if (elementValue.length < 5) {
					errorList
							.push("Length of password should be more 5 characters"
									+ "\n");
				} else {
					confirmPassword = elementValue;
				}
			}
			if (element.name == "confirmPassword") {
				if (elementValue == "") {
					errorList.push("Enter confirm password\n");
				} else if (elementValue != confirmPassword) {
					errorList.push("Passwords don't match!\n");
				}
			}
		}
	}
	if (errorEmail !== undefined) {
		errorList.push(errorEmail);
	}
	if (errorList.length == 0) {
		return true;
	} else {
		var errorMessage = 'Please\n';
		for (var i = 0; i < errorList.length; i++) {
			errorMessage = errorMessage + errorList[i];
		}
		alert(errorMessage);
	}
	return false;
}