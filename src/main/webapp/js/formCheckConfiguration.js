var flagDelete = false;
$(function() {
	$('#deleteOffice').click(function() {
		flagDelete = true;
	})
})

function onEnterPressModule(moduleId) {
	var inputNameModule = $('input[name=' + moduleId + ']');
	inputNameModule
			.keypress(function(event) {
				event.stopImmediatePropagation();
				if (event.which == 13) // the enter key code
				{
					var newModuleData = $(
							'input.modul-duration[name=' + moduleId + ']')
							.val();
					var newNameModule = $(
							'input.modul-name[name=' + moduleId + ']').val();
					if (newModuleData == '') {
						alert('Enter module duration!');
						return false;
					} else if (!(newModuleData.match(new RegExp(
							"^\\s*[1-5]\\s*$")))) {
						alert("Incorrect duration \n");
						return false;
					}
					if (newNameModule == '') {
						alert('Enter module name!');
						return false;
					}
					changeModule($.trim(newModuleData), $.trim(newNameModule),
							moduleId);
					return false;
				}
			});
}
function changeModule(newModuleDuration, newNameModule, moduleId) {
	if (confirmDialogWindow("Want to change module?")) {
		var changeModule = 'curatorChangeModule';
		$.ajax({
			url : changeModule,
			contentType : "application/json",
			type : 'POST',
			data : JSON.stringify({
				'moduleId' : moduleId,
				'duration' : newModuleDuration,
				'name' : newNameModule
			}),
			dataType : 'json',
			success : function(response) {
				$('input.modul-duration[name=' + moduleId + ']').attr(
						"onblur",
						"this.value=!this.value?" + newModuleDuration
								+ ":this.value;");
				$('input[name=' + moduleId + ']').first().attr(
						"onblur",
						"this.value=!this.value?\'" + newNameModule
								+ "\':this.value;");
				console.log("Good job!!!!!");
			},
			error : function(xhr, status, errorThrown) {
				if (xhr.status == '423') {
					alert('Not valid data!');
				} else {
					alert("Opsss...Internal server error!");
				}
			},
		});
	}
}
$(function() {
	$('.delete-group-button-configPage')
			.click(
					function() {
						if (confirmDialogWindow("Want to delete all groups?")) {
							$
									.ajax({
										url : 'curatorDeleteAllGroups',
										contentType : 'application/json',
										type : 'DELETE',
										dataType : 'json',
										success : function(response) {
											deleteMentorMessage(
													'All groups have been deleted successfully!',
													true);
											console
													.log("All groups have been deleted successfully");
										},
										error : function(xhr, status,
												errorThrown) {
											deleteMentorMessage('Error.', false);
										},
									});
						}
					})
})
$(function() {
	$('.delete-mentor-button')
			.click(
					function() {
						if (confirmDialogWindow("Want to delete all mentors?")) {
							$
									.ajax({

										url : 'curatorDeleteAllMentors',
										contentType : 'application/json',
										type : 'DELETE',
										dataType : 'json',
										success : function(response) {
											if (response == true) {
												deleteMentorMessage(
														'All mentors have been deleted successfully',
														true);
												console
														.log("Good mentor delete!!!!!");
											} else {
												deleteMentorMessage(
														'Error. Did you delete groups?',
														false);
											}
										},
										error : function(xhr, status,
												errorThrown) {
											alert("Opsss...Internal server error!");
										},
									});
						}
					})
})
function deleteMentorMessage(message, flag) {
	var info = document.getElementById('clearing-data-message');
	info.innerHTML = message;
	if (flag == true) {
		$('#clearing-data-message').css('color', 'green');
	} else {
		$('#clearing-data-message').css('color', 'red');
	}
	$('#clearing-data-message').css('opacity', 1);
	$('#clearing-data-message').animate({
		"opacity" : 0
	}, 3000);
}
$(function() {
	$('.add-office-submit')
			.click(
					function() {
						var cityId = $("#city-select option:selected").val();
						if (cityId == 0) {
							alert('Select city!');
							return false;
						}
						var segmentId = $("#segment-select option:selected")
								.val();
						if (segmentId == 0) {
							alert('Select segment!');
							return false;
						}
						var cityName = $("#city-select option:selected").text();

						var street = $('input[name="street"]').val().trim();
						if (street == '' || street == 'Enter office here...') {
							alert('Enter office name!');
							return false;
						} else {
							var address = {
								'street' : street,
								'city' : {
									'cityId' : cityId,
									'name' : cityName,
								},
								'segment' : {
									'segmentId' : segmentId
								}
							};
							$
									.ajax({

										url : 'curatorAddNewOffice',
										contentType : 'application/json',
										type : 'PUT',
										data : JSON.stringify(address),
										dataType : 'json',
										success : function(response) {
											if (response == true) {
												cancelFildNewOffice();
												getOfficeList();
												infoMessage(
														'New office has been added successfuly!',
														true);
												console
														.log("New office has been added successfuly!");
											} else {
												infoMessage(
														'This address has already created!',
														false);
											}
										},
										error : function(xhr, status,
												errorThrown) {
											if (xhr.status == '423') {
												infoMessage(
														'Error. Check address!',
														false);
											} else {
												alert("Opsss...Internal server error!");
											}
										},
									});
						}
					})
});
function infoMessage(message, flag) {
	var info = document.getElementById('info-message');
	info.innerHTML = message;
	if (flag == true) {
		$('#info-message').css('color', 'green');
	} else {
		$('#info-message').css('color', 'red');
	}
	$('#info-message').css('opacity', 1);
	$('#info-message').animate({
		"opacity" : 0
	}, 3000);
}
var getOfficeList = function() {
	var cityId = $("#city-select").val();
	var getStreetUrlById = 'getAddressiesByCityId/' + cityId;
	$.ajax({
		url : getStreetUrlById,
		contentType : "application/json",
		type : 'GET',
		dataType : 'json',
		success : function(response) {
			createOfficeSelector(response);
		},
		error : function(xhr, status, errorThrown) {
			console.log('Status: ' + status);
			console.log('Error: ' + errorThrown);
			cleanSelect();
		},
	});
}
$(function() {
	$('.delete-office-button')
			.click(
					function() {
						var cityId = $("#city-select option:selected").val();
						if (cityId == 0) {
							alert('Select city!');
							return false;
						}
						var street = $("#street-select option:selected").val();
						street = 1;
						if (0 == street) {
							alert('Select office name!');
							return false;
						} else {
							if (confirmDialogWindow("Want to delete office?")) {
								var office = {
									'street' : street
								};
								$
										.ajax({
											url : 'curatorDeleteOffice',
											contentType : 'application/json',
											type : 'POST',
											data : JSON.stringify(office),
											dataType : 'json',
											success : function(response) {
												if (response == true) {
													$(
															"#street-select option:selected")
															.remove();
													infoMessage(
															'Chosen office has been deleted successfuly!',
															true);
													console
															.log("Good office delete!!!!!");
												} else {
													infoMessage(
															'Oppps...Did you delete users or groups?',
															false);
												}
											},
											error : function(xhr, status,
													errorThrown) {
												if (xhr.status == '423') {
													infoMessage(
															'Error. Check address!',
															false);
												} else {
													alert("Opsss...Internal server error!");
												}
											},
										});
							}
						}
					})
});
$(function() {
	$('#addModule')
			.click(
					function() {
						var modulName = $('input[name="add-modul-name"]').val();
						var duration = $(
								'input[name="duration-module"]:checked').val();
						if (modulName == '') {
							alert('Enter module name!');
							return false;
						} else {
							$
									.ajax({
										url : 'curatorAddModule',
										contentType : "application/json",
										type : 'PUT',
										data : JSON.stringify({
											'duration' : duration,
											'name' : modulName
										}),
										dataType : 'json',
										success : function(response) {
											$('tbody')
													.prepend(
															'<tr><td><input class="modul-name" type="text" value="'
																	+ modulName
																	+ '" name='
																	+ response
																	+ ' onkeydown="onEnterPressModule(\''
																	+ response
																	+ '\')" onfocus="this.select()" onblur="this.value=!this.value?\''
																	+ modulName
																	+ '\':this.value;" /></td><td><input class="modul-duration" type="text" value='
																	+ duration
																	+ ' name="'
																	+ response
																	+ '" onkeydown="onEnterPressModule(\''
																	+ response
																	+ '\')" onfocus="this.select()" onblur="this.value=!this.value?"'
																	+ duration
																	+ '":this.value;" /></td><td><input type="checkbox" name="deleteGroup" value="'
																	+ response
																	+ '"></td></tr>');
											console.log("Good modul add!!!!!");
											$('input[name="add-modul-name"]')
													.val('');
											$(
													'input[name="duration-module"]:checked')
													.prop('checked', false);
											$('#first-duration').prop(
													'checked', true);
										},
										error : function(xhr, status,
												errorThrown) {
											alert('Oppps... Error: '
													+ errorThrown);
										},
									});
						}
					})
});
$(function() {
	$('#deleteModule').click(deleteModule)

})
$(function() {
	$('#deleteModuleFixed').click(deleteModule)

})
$(function() {
	$('#flag').click(registrationPeriod)

})

function registrationPeriod() {
	var box = confirmDialogWindow("Are you sure you want to do this?");
	if (box == true) {
		if ($('#flag').is(':checked')) {
			$('#flag').attr('checked', true);
			curatorRegistrationPeriodSwitcher(true);
		} else {
			$('#flag').removeAttr('checked');
			curatorRegistrationPeriodSwitcher(false);
		}
	} else {
		return false;
	}

}

function curatorRegistrationPeriodSwitcher(flag) {
	$.ajax({
		url : 'curatorRegistrationPeriodSwitcher',
		contentType : "application/json",
		type : 'POST',
		data : JSON.stringify(flag),
		dataType : 'json',
		success : function() {
			var locationOffice = document
					.getElementById('message-flag-checkbox');
			if (flag) {
				var output = '<h9>Users can register</h9>';
				locationOffice.innerHTML = output;
			} else {
				var output = '<h9>Users can\'t register</h9>';
				locationOffice.innerHTML = output;
			}
		},
	});
}

function deleteModule() {
	var listDeleteModul = $('table input[type=checkbox]:checked').map(
			function(_, el) {
				return $(el).val();
			}).get();
	if (confirmDialogWindow("Want to delete module?")) {
		$.ajax({
			url : 'curatorDeleteModuleByListId',
			contentType : "application/json",
			type : 'DELETE',
			data : JSON.stringify(listDeleteModul),
			dataType : 'json',
			success : function(data, textStatus, jqXHR) {
				if (data == true) {
					for (var i = 0; i < listDeleteModul.length; i++) {
						$('input[name=' + listDeleteModul[i] + ']').closest(
								'tr').remove();
					}
				} else {
					alert('Oppps... Did you delete groups?');
				}
			},
			error : function(xhr, status, errorThrown) {
				alert("Opsss...Internal server error!");
			},
		});
	}
}

$(document)
		.ready(
				function fildNewOffice() {
					$("#addOffice")
							.click(
									function() {
										var locationOffice = document
												.getElementById('location-office');
										var output = '<input type="text" name="street" onclick="this.value=\'\';" onfocus="this.select()" onblur="this.value=!this.value?\'Enter office here...\':this.value;"value="Enter office here..." />';
										locationOffice.innerHTML = output;
										$('.delete-office-button').hide();
										$('.add-office-button').hide();
										$('.add-office-submit').show('slow');
										$('.add-office-cancel').show('slow');
										$('.add-office-submit').css('display',
												'inline');
										$('.add-office-cancel').css('display',
												'inline');
										$('.segment').css('display',
												'inline-block');

									});
				});
$(function() {
	$("#addOffice-cansel").click(function() {
		cancelFildNewOffice()
	});
});
var cancelFildNewOffice = function() {
	var locationOffice = document.getElementById('location-office');
	var output = '<label> <select id="street-select" name="street" data-default-value><option value="0">-- Select office --</option></select></label>';
	locationOffice.innerHTML = output;
	getOfficeList();
	$('.delete-office-button').show('slow');
	$('.add-office-button').show('slow');
	$('.add-office-submit').hide();
	$('.add-office-cancel').hide();
	$('.segment').hide('slow');
	$('#info-message').empty();
};
